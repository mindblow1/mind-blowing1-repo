#!/bin/bash
#PBS -N RF-classify
#PBS -M andrew.warrington@keble.ox.ac.uk
#PBS -m abe
#PBS -q laura


# Set output and error directories
#PBS -o localhost:/scratch/andreww/3yp_2016/3yp_2016/Reports/results_${PBS_JOBID}.out
#PBS -e localhost:/scratch/andreww/3yp_2016/3yp_2016/Reports/results_${PBS_JOBID}.err

cd ..

# Ensure job was launched correctly in PBS -o directory.
#echo $par_1 

# Launch matlab and change directory to folder containing matlab script, then run. 
# Parameter is passed in as a string and hence str2num must be applied inside matlab script.
/usr/local/bin/matlab -nodesktop -nodisplay -nosplash -noFigureWindows -r "cd '/scratch/andreww/3yp_2016/3yp_2016/' ; rf() ;"
