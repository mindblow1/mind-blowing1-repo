#!/bin/sh

#PBS -N clusterRunCCFandLDA
#PBS -M andrew.warrington@keble.ox.ac.uk
#PBS -m abe
#PBS -q laura
#PBS -o localhost:/scratch/andreww/errors/callSmcDecisionTree_${PBS_JOBID}.out
#PBS -e localhost:/scratch/andreww/errors/callSmcDecisionTree_${PBS_JOBID}.err

#$ -N clusterRunCCFandLDA
#$ -M twgr@robots.ox.ac.uk
#$ -m bea
#$ -cwd
#$ -V    
#$ -o localhost:/scratch/twgr/ccf/log/callSmcDecisionTree_${PBS_JOBID}.out
#$ -e localhost:/scratch/twgr/ccf/log/callSmcDecisionTree_${PBS_JOBID}.err

OUTPUTBASEDIR=/scratch/twgr/ccf/

if [ -n "$PBS_JOBID" ]; then
  # sanitize job id
  JOBNUM=`echo $PBS_JOBID | sed -e "s#\(^[0-9]\{1,\}\).*#\1#g"`
  if [ -z $PBS_ARRAYID ]
  then
      JOBID=$JOBNUM
  else
      JOBID=`printf "%d_%04d" ${JOBNUM} ${PBS_ARRAYID}`
  fi
elif [ -n "$SGE_TASK_ID" ]; then
  JOBNUM=$SGE_TASK_ID
  JOBID=$JOBNUM
elif [ -z "$JOBID" ]; then
  JOBNUM=`date +%y%m%d%H%M`
  JOBID=$JOBNUM
fi

# run code\
cd $OUTPUTBASEDIR
#/homes/51/twgr/functioninduction/Tom/run_callSmcDecisionTree.sh /usr/local/MATLAB/R2014a/ $OUTPUTBASEDIR/data
/usr/local/bin/matlab -nodesktop -nodisplay -nosplash -noFigureWindows -r "cd /homes/51/twgr/functioninduction/Tom/; clusterRunCCFandLDA('/scratch/Tom/RandomFolds/');"

#touch test${PBS_ARRAYID}
# cd ${output_dir}
# java -jar -Xmx5500m \
#  ${jar_file} \
#  -r ${JOBNUM} \
#  -P 100 \
#  -n 20000 \
#  -c \
#  -m cascade \
#  -s "${output_dir}/gaussian-with-unknown-mean.venture" \
#  -p "${output_dir}/${script_name}_${JOBID}.predict"
