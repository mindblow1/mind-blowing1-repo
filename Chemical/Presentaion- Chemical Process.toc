\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Necessity of brain chemical processing and main concepts involved}{3}{0}{1}
\beamer@sectionintoc {2}{Introducing main fixation and embedment methods}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Osmium Tetroxide Perfusion+ improvement}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Resin Embedment}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Cryopreservation by Vitrification}{9}{0}{2}
\beamer@subsectionintoc {2}{4}{Aldehyde-Stabilized Cryopreservation}{11}{0}{2}
\beamer@subsectionintoc {2}{5}{Comparison and Summary}{13}{0}{2}
\beamer@sectionintoc {3}{Introducing main staining methods}{14}{0}{3}
\beamer@subsectionintoc {3}{1}{Lead Citrate}{15}{0}{3}
\beamer@subsectionintoc {3}{2}{DMSO}{17}{0}{3}
\beamer@subsectionintoc {3}{3}{WbPATCO Staining}{18}{0}{3}
\beamer@subsectionintoc {3}{4}{Comparison and Summary}{20}{0}{3}
\beamer@sectionintoc {4}{Choice-making and Conclusion}{21}{0}{4}
