\contentsline {section}{\numberline {1}Introduction}{6}
\contentsline {subsection}{\numberline {1.1}A Brief Introduction Of The Design Project}{6}
\contentsline {subsection}{\numberline {1.2}What Is A Connectome}{6}
\contentsline {subsection}{\numberline {1.3}Implications Of Construction Of Human Connectome}{7}
\contentsline {subsection}{\numberline {1.4}Challenges Of Building A Connectome}{8}
\contentsline {subsection}{\numberline {1.5}A Solution Proposed For Human Connectome Construction}{9}
\contentsline {section}{\numberline {2}Tissue Preparation}{11}
\contentsline {subsection}{\numberline {2.1}The Necessity Of Chemical Process}{11}
\contentsline {subsection}{\numberline {2.2}Fixation}{12}
\contentsline {subsubsection}{\numberline {2.2.1}ASC Procedure}{12}
\contentsline {paragraph}{\numberline {2.2.1.1}Bilateral Carotid Cannulation}{12}
\contentsline {paragraph}{\numberline {2.2.1.2}Fixation}{13}
\contentsline {paragraph}{\numberline {2.2.1.3}Computer Transfer}{14}
\contentsline {paragraph}{\numberline {2.2.1.4}CPA Introduction}{14}
\contentsline {paragraph}{\numberline {2.2.1.5}Vitrification}{14}
\contentsline {subsubsection}{\numberline {2.2.2}Solutions Used And Scale Table}{14}
\contentsline {subsubsection}{\numberline {2.2.3}Alternative Fixation Protocols}{17}
\contentsline {paragraph}{\numberline {2.2.3.1}Immersion}{17}
\contentsline {paragraph}{\numberline {2.2.3.2}Perfusion}{17}
\contentsline {subsubsection}{\numberline {2.2.4}Retrieving The Brain}{17}
\contentsline {subsubsection}{\numberline {2.2.5}Preparation For Staining}{18}
\contentsline {subsection}{\numberline {2.3}Thick Sectioning}{19}
\contentsline {subsubsection}{\numberline {2.3.1}Tissue Chopper}{19}
\contentsline {subsubsection}{\numberline {2.3.2}Vibratome}{20}
\contentsline {subsubsection}{\numberline {2.3.3}Thick-Strip Sectioning And Tape Collection Machine}{21}
\contentsline {subsubsection}{\numberline {2.3.4}Comparison}{23}
\contentsline {subsection}{\numberline {2.4}Staining}{24}
\contentsline {subsubsection}{\numberline {2.4.1}BROPA Staining Protocol}{24}
\contentsline {paragraph}{\numberline {2.4.1.1}Removing Embedment And Rinsing}{25}
\contentsline {paragraph}{\numberline {2.4.1.2}Staining}{25}
\contentsline {paragraph}{\numberline {2.4.1.3}Dehydration}{26}
\contentsline {subsubsection}{\numberline {2.4.2}Alternative Staining Protocols}{26}
\contentsline {paragraph}{\numberline {2.4.2.1}wbPATCO}{26}
\contentsline {paragraph}{\numberline {2.4.2.2}rOTO}{26}
\contentsline {paragraph}{\numberline {2.4.2.3}Other Protocols}{26}
\contentsline {subsubsection}{\numberline {2.4.3}Alternative Neuron-Tracing Strategies}{26}
\contentsline {paragraph}{\numberline {2.4.3.1}Brainbow}{27}
\contentsline {paragraph}{\numberline {2.4.3.2}MAPseq}{28}
\contentsline {paragraph}{\numberline {2.4.3.3}SeeDB2}{30}
\contentsline {subsection}{\numberline {2.5}Embedding}{30}
\contentsline {subsubsection}{\numberline {2.5.1}An Improved Spurr's Protocol}{31}
\contentsline {subsubsection}{\numberline {2.5.2}Scaling Of Embedding Media}{32}
\contentsline {subsubsection}{\numberline {2.5.3}Alternative Embedding Media}{33}
\contentsline {paragraph}{\numberline {2.5.3.1}Acrylic Resin}{33}
\contentsline {paragraph}{\numberline {2.5.3.2}Polyester}{33}
\contentsline {subsection}{\numberline {2.6}Further Consideration For Automation}{33}
\contentsline {subsection}{\numberline {2.7}Ultrathin Sectioning}{36}
\contentsline {subsubsection}{\numberline {2.7.1}Serial Block Face Scanning Electron Microscopy}{38}
\contentsline {subsubsection}{\numberline {2.7.2}Knife-Edge Scanning Microscope}{39}
\contentsline {subsubsection}{\numberline {2.7.3}Focused Ion Beam}{40}
\contentsline {subsubsection}{\numberline {2.7.4}Automated Tape-Collecting Ultramicrotome}{43}
\contentsline {subsubsection}{\numberline {2.7.5}Comparison}{45}
\contentsline {subsubsection}{\numberline {2.7.6}Other Considerations}{46}
\contentsline {paragraph}{\numberline {2.7.6.1}Diamond Knife Resharpening}{46}
\contentsline {paragraph}{\numberline {2.7.6.2}Empty Sections}{47}
\contentsline {subsection}{\numberline {2.8}Risk Consideration}{47}
\contentsline {subsection}{\numberline {2.9}Time Consideration For Tissue Preparation}{51}
\contentsline {subsection}{\numberline {2.10}Cost Consideration For Tissue Preparation}{52}
\contentsline {section}{\numberline {3}Imaging}{54}
\contentsline {subsection}{\numberline {3.1}Live Imaging}{54}
\contentsline {subsubsection}{\numberline {3.1.1}Magnetic Resonance Imaging (MRI)}{55}
\contentsline {subsubsection}{\numberline {3.1.2}Computational Tomography (CT)}{56}
\contentsline {subsection}{\numberline {3.2}Nano-Scale MRI And CT}{56}
\contentsline {subsection}{\numberline {3.3}Light Microscopy}{58}
\contentsline {subsubsection}{\numberline {3.3.1}Near-Field Scanning Optical Microscopy (NSOM)}{59}
\contentsline {subsubsection}{\numberline {3.3.2}Stimulated Emission Depletion Microscopy (STED)}{60}
\contentsline {subsubsection}{\numberline {3.3.3}STORM And PALM}{60}
\contentsline {subsubsection}{\numberline {3.3.4}Extra Considerations}{61}
\contentsline {subsubsection}{\numberline {3.3.5}Comparison Of Light Microscopy Techniques}{62}
\contentsline {subsection}{\numberline {3.4}Electron Microscopy}{63}
\contentsline {subsubsection}{\numberline {3.4.1}Transition Electron Microscopy}{63}
\contentsline {subsubsection}{\numberline {3.4.2}Scanning Electron Microscopy}{64}
\contentsline {subsubsection}{\numberline {3.4.3}MultiSEM 506}{66}
\contentsline {subsubsection}{\numberline {3.4.4}Comparison Of EM}{66}
\contentsline {subsection}{\numberline {3.5}Analysis}{67}
\contentsline {subsection}{\numberline {3.6}Scanning Process And Maintenance}{67}
\contentsline {subsection}{\numberline {3.7}Future Development}{69}
\contentsline {section}{\numberline {4}Data Processing Software}{70}
\contentsline {subsection}{\numberline {4.1}Introduction}{70}
\contentsline {subsection}{\numberline {4.2}Processing Algorithm}{70}
\contentsline {subsubsection}{\numberline {4.2.1}Alignment}{71}
\contentsline {subsubsection}{\numberline {4.2.2}Segmentation}{71}
\contentsline {subsubsection}{\numberline {4.2.3}Segment Classification}{74}
\contentsline {subsubsection}{\numberline {4.2.4}Skeletonisation}{76}
\contentsline {subsection}{\numberline {4.3}Training}{79}
\contentsline {subsection}{\numberline {4.4}Storage Format}{80}
\contentsline {subsection}{\numberline {4.5}Merging Blocks}{81}
\contentsline {subsection}{\numberline {4.6}Performance}{81}
\contentsline {section}{\numberline {5}Data Processing Hardware}{83}
\contentsline {subsection}{\numberline {5.1}Introduction}{83}
\contentsline {subsection}{\numberline {5.2}Processors}{84}
\contentsline {subsubsection}{\numberline {5.2.1}Tensor Processing Units}{84}
\contentsline {subsubsection}{\numberline {5.2.2}Graphics Processing Units}{86}
\contentsline {subsubsection}{\numberline {5.2.3}Central Processing Units}{86}
\contentsline {subsection}{\numberline {5.3}Short-Term Storage}{87}
\contentsline {subsection}{\numberline {5.4}Long-Term Storage}{90}
\contentsline {subsubsection}{\numberline {5.4.1}Comparison Of Storage Media}{90}
\contentsline {subsubsection}{\numberline {5.4.2}Data Backup}{91}
\contentsline {subsubsection}{\numberline {5.4.3}Tape Hardware Requirements}{93}
\contentsline {subsection}{\numberline {5.5}Data Hardware Summary}{95}
\contentsline {section}{\numberline {6}Business Case}{97}
\contentsline {section}{\numberline {7}Challenge Problem}{98}
\contentsline {subsection}{\numberline {7.1}Overview Of The Structure}{98}
\contentsline {subsection}{\numberline {7.2}Training}{99}
\contentsline {subsection}{\numberline {7.3}Integrating Challenge Problem With The Process}{100}
\contentsline {subsubsection}{\numberline {7.3.1}Detection Only On Boundaries}{100}
\contentsline {subsubsection}{\numberline {7.3.2}Grouping Synapse Voxels}{100}
\contentsline {section}{\numberline {8}Other Consideration}{102}
\contentsline {subsection}{\numberline {8.1}Connectome Theory}{102}
\contentsline {subsection}{\numberline {8.2}Legal Consideration}{102}
\contentsline {subsection}{\numberline {8.3}Ethical And Social Consideration}{103}
