tree1 = [44, 22; 41, 22; 37, 20; 32, 19; 27, 16]';
tree2 = [9, 7; 11, 9; 15, 10; 19, 12; 22, 14]';
tree3 = [0, 24; 2, 22; 6, 20; 9, 20; 13, 19; 17, 18; 19, 16]';

c1 = [27 22; 16 14];
c2 = c1;
c3 = [19 19; 16 12];

linewidth = 3;

hold on
plot(tree1(1,:), tree1(2,:),'k','LineWidth',linewidth)
plot(tree2(1,:), tree2(2,:),'k','LineWidth',linewidth)
plot(tree3(1,:), tree3(2,:),'k','LineWidth',linewidth)

plot(c1(1,:), c2(2,:),'k','LineWidth',linewidth)
plot(c3(1,:), c3(2,:),'k','LineWidth',linewidth)

set(gca, 'Ydir', 'reverse')
axis([0, 44, 0, 29])
axis equal