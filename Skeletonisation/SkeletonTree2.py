# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 20:58:31 2017

@author: user
"""

from PIL import Image
import numpy as np

MedAxis = Image.open('DLMA2.bmp')
MedAxis = np.array(MedAxis)[:,:,0]
height, width = MedAxis.shape

edges_image = Image.open('segment2_edges.bmp')
edges = np.array(edges_image)

def distanceBetween(coord1, coord2):
    x1, y1 = coord1
    x2, y2 = coord2
    return ((x1-x2)**2+(y1-y2)**2)**0.5

def withinSphere(point, centre, radius):
    return distanceBetween(point, centre) <= radius

def smallestSphere(points):
    """ using Ritters approximation algorithm """
    #print(points)
    if len(points)<2:
        return 0
    x = points[0]
    biggestDist=0
    for point in points:
        if distanceBetween(x,point)>biggestDist:
            biggestDist = distanceBetween(x,point)
            y = point
    biggestDist=0
    for point in points:
        if distanceBetween(y,point)>biggestDist:
            biggestDist = distanceBetween(y,point)
            z = point
    poss_centre = ((y[0]+z[0])/2, (y[1]+z[1])/2)
    poss_radius = 0.5 * distanceBetween(y,z)    
    for point in points:
        if not withinSphere(point, poss_centre, poss_radius):
            error = distanceBetween(point, poss_centre) - poss_radius
            poss_centre = (poss_centre[0]+0.5*(point[0]-poss_centre[0]), poss_centre[1]+0.5*(point[1]-poss_centre[1]))
            poss_radius = poss_radius + error*0.5
    return poss_radius

edge1 = []
edge3 = []
for x in range(0, int(width/2)):     #tells which edge is which based on where it is 
                                # -wouldn't work but is fine for example
    for y in range(0, int(height/2)):
        if edges[y, x]:
            edge1.append((x, y))
    
    for y in range(int(height/2), height):
        if edges[y, x]:
            edge3.append((x, y))
            
edge2 = []
for x in range(int(width/2), width):
    for y in range(0, height):
        if edges[y, x]:
            edge2.append((x, y))

skeleton_voxels = {}
for x in range(width):
    for y in range(height):
        if MedAxis[y, x]:
            skeleton_voxels[(x, y)] = MedAxis[y, x]

edgeWidths = []
edges = [edge3, edge2, edge1]
trees = [[],[],[]] #a tree associated with each edge
tree_available_nodes = [skeleton_voxels.copy(), skeleton_voxels.copy(), skeleton_voxels.copy()]

""" Find the first node for each leg* of the tree   *trees have legs """
for edge_no in range(len(edges)):
    edge = edges[edge_no]
    maxDistance = 0
    for a_pos in edge:
        for b_pos in skeleton_voxels:
            if distanceBetween(a_pos, b_pos) > maxDistance and distanceBetween(a_pos, b_pos) <= skeleton_voxels[b_pos]+1:
                maxDistance = distanceBetween(a_pos, b_pos)
                bestEdgeNugget = a_pos
                bestBone = b_pos
    trees[edge_no].append(bestEdgeNugget)
    edgeWidths.append(smallestSphere(edge))
    skeleton_voxels[bestEdgeNugget] = round(smallestSphere(edge))
    trees[edge_no].append(bestBone)

completed_trees = []
connections = []

def touchingTree(tree, all_trees):
    head = tree[-1]
    for other_tree in all_trees:
        if other_tree[0] != tree[0]:
            for node in other_tree:
                if distanceBetween(node, head) < skeleton_voxels[node] + skeleton_voxels[head] + 1:
                    # head of tree is touching other_tree so tree is completed
                    return node
    return False

""" Find more nodes until the trees meet """
while len(trees) > 0:
    for tree_no in range(len(trees)):
        tree = trees[tree_no]
        availableNodes = tree_available_nodes[tree_no].copy()
        freeNode = tree[-1]
        freeRadius = skeleton_voxels[freeNode]
        
        furthestDistance = 0
        furthestPossibility = 0
        for potentialNewNode in availableNodes:
            if availableNodes[potentialNewNode]+freeRadius+1 > distanceBetween(potentialNewNode, freeNode):
                if distanceBetween(potentialNewNode, freeNode) + availableNodes[potentialNewNode] > furthestDistance:
                    furthestDistance = distanceBetween(potentialNewNode, freeNode) + availableNodes[potentialNewNode]
                    furthestPossibility = potentialNewNode
                tree_available_nodes[tree_no].pop(potentialNewNode)    #remove so wont be considered again whether it ends up being chosen or not
        if furthestPossibility == 0:
            raise TypeError('tree broken')
        tree.append(furthestPossibility)
     
    trees_snapshot = trees.copy()
    all_trees = trees + completed_trees
    for tree in trees_snapshot:
        #check if head of tree is touching any nodes in any other tree
        # if it is, send it to completed trees but keep the other tree-
        # other tree will be deleted whenever its head touches something- possibly this tree
        joint = touchingTree(tree, all_trees)
        if joint:
            completed_trees.append(tree)
            connections.append((tree[-1], joint))
            trees.remove(tree)




full_tree = sum(completed_trees, [])

canvas = np.zeros((height, width, 3), 'uint8')
#canvas = np.array(edges_image.convert('RGB'))
for edge in edges:
    for point in edge:
        canvas[point[1], point[0]] = [255,0,0];
for tree in completed_trees:
    for node in tree:
        canvas[node[1], node[0]] = [0,0,255];
canvas = Image.fromarray(canvas)
canvas.save('segment2_nodes.bmp')

paper = np.zeros((height, width))
for node in full_tree:
    r = skeleton_voxels[node]
    x, y = node
    for x_off in range(0-r, r+1):
        y_lim = int((r**2-x_off**2)**0.5)
        for y_off in range(-y_lim, y_lim+1):
            try:
                paper[y+y_off, x+x_off] = 255
            except:
                pass
Paper = Image.fromarray(paper)