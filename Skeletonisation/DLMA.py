# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 16:41:38 2017

@author: user
"""

from PIL import Image
import numpy as np

DT = Image.open('DT2.bmp')
DT = DT.convert('F')
DT = np.array(DT)

Shape = Image.open('segment2.bmp')
Shape = np.array(Shape)
height, width = Shape.shape

def neighbours(coord):
    x, y = coord
    return [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]

def squaredDistanceToBoundary(coord):
    x, y = coord
    return DT[y, x]

def distanceBetween(coord1, coord2):
    x1, y1 = coord1
    x2, y2 = coord2
    return ((x1-x2)**2+(y1-y2)**2)**0.5

def withinSphere(point, centre, radius):
    return distanceBetween(point, centre) <= radius

def inShape(coord):
    return Shape[coord[1], coord[0]]

def inImage(coord):
    return coord[0] > 0 and coord[1] > 0 and coord[0] < width and coord[1] < height

def pi(point):
    """ return set of points on external boundary closest to point """
    assert(inShape(point))
    boundaryPointSet = []
    radius2 = squaredDistanceToBoundary(point)
    x, y = point
    for x_offset in range(int(-radius2**0.5), int(radius2**0.5)+1):
        y_offset_magnitude = int((radius2 - x_offset**2)**0.5)
        for y_offset in range(-y_offset_magnitude, y_offset_magnitude+1):
            if inImage((x+x_offset, y+y_offset)) and not inShape((x+x_offset, y+y_offset)):
                boundaryPointSet.append((x+x_offset, y+y_offset))
    return boundaryPointSet

def pi_e(point):
    boundaryPointSet = pi(point)
    for y in neighbours(point):
        if inImage(y) and squaredDistanceToBoundary(y) <= squaredDistanceToBoundary(point):
            if inShape(y):
                subset = pi(y)
            else:
                subset = []
            for element in subset:
                if element not in boundaryPointSet:
                    boundaryPointSet.append(element)
    return boundaryPointSet

def smallestSphere(points):
    """ using Ritters approximation algorithm """
    #print(points)
    if len(points)<2:
        return 0
    x = points[0]
    biggestDist=0
    for point in points:
        if distanceBetween(x,point)>biggestDist:
            biggestDist = distanceBetween(x,point)
            y = point
    biggestDist=0
    for point in points:
        if distanceBetween(y,point)>biggestDist:
            biggestDist = distanceBetween(y,point)
            z = point
    poss_centre = ((y[0]+z[0])/2, (y[1]+z[1])/2)
    poss_radius = 0.5 * distanceBetween(y,z)    
    for point in points:
        if not withinSphere(point, poss_centre, poss_radius):
            error = distanceBetween(point, poss_centre) - poss_radius
            poss_centre = (poss_centre[0]+0.5*(point[0]-poss_centre[0]), poss_centre[1]+0.5*(point[1]-poss_centre[1]))
            poss_radius = poss_radius + error*0.5
    return poss_radius

def F(point):
    if inShape(point):
        return smallestSphere(pi_e(point))
    else:
        return 0
    
lambda_ = 1.8

F_map = np.zeros(Shape.shape)
for x in range(width):
    for y in range(height):
        point = (x, y)
        F_map[y, x] = F(point) * (F(point) > lambda_)   #value at voxel is now radius of sphere

MedAxis = Image.fromarray(F_map)
MedAxis = MedAxis.convert('RGB')
MedAxis.save('DLMA2.bmp')

M2 = Image.fromarray(F_map*255/F_map.max())
M2 = M2.convert('RGB')
M2.show()