# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 20:58:31 2017

@author: user
"""

from PIL import Image
import numpy as np

MedAxis = Image.open('DLMA.bmp')
MedAxis = np.array(MedAxis)[:,:,0]
height, width = MedAxis.shape

edges_image = Image.open('segment_edges.bmp')
edges = np.array(edges_image)

def distanceBetween(coord1, coord2):
    x1, y1 = coord1
    x2, y2 = coord2
    return ((x1-x2)**2+(y1-y2)**2)**0.5

edge1 = []
for x in range(0, int(width/2)):     #tells which edge is which based on where it is 
                                # -wouldn't work but is fine for example
    for y in range(0, height):
        if edges[y, x]:
            edge1.append((x, y))
            
edge2 = []
for x in range(int(width/2), width):
    for y in range(0, height):
        if edges[y, x]:
            edge2.append((x, y))

skeleton_voxels = {}
for x in range(width):
    for y in range(height):
        if MedAxis[y, x]:
            skeleton_voxels[(x, y)] = MedAxis[y, x]

edges = [edge1, edge2]
trees = [[],[]] #a tree associated with each edge
tree_available_nodes = [skeleton_voxels.copy(), skeleton_voxels.copy()]

for edge_no in range(len(edges)):
    edge = edges[edge_no]
    minDistance = 10000
    for a_pos in edge:
        for b_pos in skeleton_voxels:
            if distanceBetween(a_pos, b_pos) < minDistance:
                minDistance = distanceBetween(a_pos, b_pos)
                bestBone = b_pos
    trees[edge_no].append(bestBone)

while len(trees) > 1:
    for tree_no in range(len(trees)):
        tree = trees[tree_no]
        availableNodes = tree_available_nodes[tree_no].copy()
        freeNode = tree[-1]
        freeRadius = skeleton_voxels[freeNode]
        
        furthestDistance = 0
        for potentialNewNode in availableNodes:
            if availableNodes[potentialNewNode]+freeRadius > distanceBetween(potentialNewNode, freeNode):
                if distanceBetween(potentialNewNode, freeNode) > furthestDistance:
                    furthestDistance = distanceBetween(potentialNewNode, freeNode)
                    furthestPossibility = potentialNewNode
                tree_available_nodes[tree_no].pop(potentialNewNode)    #remove so wont be considered again whether it ends up being chosen or not
        tree.append(furthestPossibility)
    
    #check if any trees are touching - merge if they are
    tree1 = trees[0]
    tree2 = trees[1]
    end1 = tree1[-1]
    end2 = tree2[-1]
    if distanceBetween(end1, end2) < skeleton_voxels[end1] + skeleton_voxels[end2]:
        trees.remove(tree1)
        trees.remove(tree2)
        trees.append(tree1+tree2[:-1])



#keep repeating this but first check if any trees are touching

canvas = np.zeros((height, width, 3), 'uint8')
#canvas = np.array(edges_image.convert('RGB'))
for edge in edges:
    for point in edge:
        canvas[point[1], point[0]] = [255,0,0];
for tree in trees:
    for node in tree:
        canvas[node[1], node[0]] = [0,0,255];
canvas = Image.fromarray(canvas)
canvas.save('segment_nodes.bmp')