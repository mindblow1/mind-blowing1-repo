# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 14:31:07 2017

@author: user
"""

from PIL import Image
import numpy as np

img = Image.open('segment2.bmp')
img = np.array(img)
height, width = img.shape

DT = np.zeros(img.shape)

for x in range(width):
    for y in range(height):
        if img[y, x]:
            DT[y, x] = 100
            for x_offset in range(-5, 6):
                for y_offset in range(-5, 6):
                    try:
                        if not img[y+y_offset, x+x_offset] and (x_offset**2+y_offset**2<DT[y, x]):
                            DT[y, x] = x_offset**2+y_offset**2
                    except IndexError:
                        pass

DT = Image.fromarray(DT)
DT = DT.convert('RGB')
DT.save('DT2.bmp')