# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 23:03:39 2017

@author: user
"""
name = 'cpu_test_net_deep'
import dataInputNoPIL as dataInput
import numpy as np
import tensorflow as tf
from CNN import *
sess = tf.InteractiveSession()

###### SET PARAMETERS
trainingStepSize = 1e-4
numIterations = 20000
sizeBatches = 100
proportionSynapses = 0.05
n = 5 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 0#vertical length is 2n+1

global threeD 
threeD = nz>0

####    LOAD ALL THE WEIGHTS AND BIASES

weights = np.load('saved_nets/weights_for_'+name+'.npz')
names = weights.items()[0][1]
values = weights.items()[1][1]
for i in range(len(names)):
    var_name = names[i]
    array = names[i]
    #exec(name + ' = values[i]')
    exec(var_name + ' = tf.Variable(values[i])')

if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)
if threeD:
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1,2*nz+1, 1])
else:
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])
h_conv1 = tf.nn.relu(conv(x_image, W_conv1_o, threeD) + b_conv1_o)    #is now 8x8(x32)
h_conv2 = tf.nn.relu(conv(h_conv1, W_conv2_o) + b_conv2_o)
h_conv3 = tf.nn.relu(conv(h_conv2, W_conv3_o) + b_conv3_o)


h_conv1_flat = tf.reshape(h_conv3, [-1, 4*4*128])

h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1_o) + b_fc1_o)
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
h_fc2 = tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o
y_conv = tf.matmul(h_fc2, W_fc3_o) + b_fc3_o

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.initialize_all_variables())




#### LOAD TRAINING DATA

EMimage = np.load("../cnn_files/npy_train/image.npy")
synapses = np.load("../cnn_files/npy_train/synapse.npy")
synapseX, not_synapseX = dataInput.createNeighbourSets(EMimage, synapses, n, nz = nz, steps = (10,10,2))
del EMimage, synapses

for i in range(numIterations):
  batch = dataSample(not_synapseX, synapseX, sizeBatches, proportionSynapses, n, nz)
  if i%100 == 0:
    train_accuracy = accuracy.eval(feed_dict={x:batch[0], y1: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y1: batch[1], keep_prob: 0.5})
  
#### SAVE MODEL
to_save = []
names = []
weights = locals().copy()
for var_name in weights:
    if var_name[-2:] == '_o':
        exec('to_save.append(' + var_name + '.eval())')
        names.append(var_name)
        
np.savez('saved_nets/weights_for_'+name, to_save, names)
print('saved')
