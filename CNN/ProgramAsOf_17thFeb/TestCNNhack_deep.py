# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 23:03:39 2017

@author: user
"""
name = 'cpu_test_net_deep'
import dataInputNoPIL as dataInput
import numpy as np
import random
import tensorflow as tf
from CNN import *
sess = tf.InteractiveSession()

###### SET PARAMETERS
#trainingStepSize = 1e-4
#numIterations = 10000
#sizeBatches = 100
#proportionSynapses = 0.05
n = 5 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 0#vertical length is 2n+1

global threeD 
threeD = nz>0

def getPredictions(sess, xData, n, nz):
    if nz>0:
        xData = np.reshape(xData, [-1, (2*nz+1), (2*n+1)**2])
    #note it doesn't matter what y1 is set to
    ans = np.ndarray([xData.shape[0],1])
    for MIN in range(0,xData.shape[0],100):
        MAX = min(MIN+100, xData.shape[0])
        result = sess.run([probSynapse], feed_dict={ x: xData[MIN:MAX], y1: [0]*100, keep_prob: 1.0})[0]
        ans[MIN:MAX] = result
    return ans


####    LOAD ALL THE WEIGHTS AND BIASES

weights = np.load('saved_nets/weights_for_'+name+'.npz')
names = weights.items()[1][1]
values = weights.items()[0][1]
for i in range(len(names)):
    var_name = names[i]
    array = values[i]
    #exec(name + ' = values[i]')
    print(type(var_name), type(array))
    exec(var_name + ' = tf.Variable(values[i])')

if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)
if threeD:
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1,2*nz+1, 1])
else:
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])
h_conv1 = tf.nn.relu(conv(x_image, W_conv1_o, threeD) + b_conv1_o)    #is now 8x8(x32)
h_conv2 = tf.nn.relu(conv(h_conv1, W_conv2_o) + b_conv2_o)
h_conv3 = tf.nn.relu(conv(h_conv2, W_conv3_o) + b_conv3_o)


h_conv1_flat = tf.reshape(h_conv3, [-1, 4*4*128])

h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1_o) + b_fc1_o)
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
h_fc2 = tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o
y_conv = tf.matmul(h_fc2, W_fc3_o) + b_fc3_o

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
#train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.initialize_all_variables())

#### LOAD TRAINING DATA


###### LOAD TEST DATA
imageTEST = np.load("../cnn_files/npy_train/imageTEST25.npy")
synapsesTEST = np.load("../cnn_files/npy_train/synapseTEST25.npy")
test_synapseX, test_not_synapseX = dataInput.createNeighbourSets(imageTEST, synapsesTEST, n, nz=nz, steps = (5,5,3))
test_synapse_predictions = getPredictions(sess, test_synapseX, n, nz)
test_not_synapse_predictions = getPredictions(sess, test_not_synapseX, n, nz)
  
bestProb = 0.3

######## CALCULATE F1 ON TEST DATA
F1, TP, FN, FP, TN = calcF1(test_synapse_predictions, test_not_synapse_predictions, bestProb, andMore=True)
print('for test data at best prob, TP:', TP, 'FN:', FN, 'FP:', FP, 'TN:', TN, 'F1:', F1, '\n')
print(len(test_synapse_predictions), len(test_not_synapse_predictions))
