# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 22:05:40 2017

@author: user
"""
import numpy as np
import tensorflow as tf

trainingStepSize = 0.001;

name = "cpu_test_net_deep"
n = 5 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 0#vertical length is 2n+1

global threeD 
threeD = nz>0

def makeDoubleVector(y1):
    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
    y1 = tf.reshape(y1, [-1, 1])
    yNot = tf.logical_not(tf.cast(y1, tf.bool))
    yNot = tf.cast(yNot, tf.float32)
    return tf.concat(1, [yNot, y1])

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv(x, W):
    if threeD:
        return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='VALID')
    else:
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')


sess = tf.InteractiveSession()

##### DEFINE CNN
if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)

if threeD:
    W_conv1_o = weight_variable([2*n-6, 2*n-6, 2*nz+1, 1, 32])            #should really adjust size with n
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1,2*nz+1, 1])
else:
    W_conv1_o = weight_variable([2*n-6, 2*n-6, 1, 32])            #should really adjust size with n
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])
b_conv1_o = bias_variable([32])

h_conv1 = tf.nn.relu(conv(x_image, W_conv1_o) + b_conv1_o)    #is now 8x8(x32)

W_conv2_o = weight_variable([2*n-7, 2*n-7, 32,64]) # now 6*6 * 64
b_conv2_o = bias_variable([64])

h_conv2 = tf.nn.relu(conv(h_conv1, W_conv2_o) + b_conv2_o)

W_conv3_o = weight_variable([2*n-7, 2*n-7, 64,128]) # now 4*4*128
b_conv3_o = bias_variable([128])

h_conv3 = tf.nn.relu(conv(h_conv2, W_conv3_o) + b_conv3_o)



W_fc1_o = weight_variable([4 * 4 * 128, 2048])
b_fc1_o = bias_variable([2048])

h_conv1_flat = tf.reshape(h_conv3, [-1, 4*4*128])
h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1_o) + b_fc1_o)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2_o = weight_variable([2048, 2])
b_fc2_o = bias_variable([2])

h_fc2 = tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o

W_fc3_o = weight_variable([2, 2])
b_fc3_o = bias_variable([2])

y_conv = tf.matmul(h_fc2, W_fc3_o) + b_fc3_o

#probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
#probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
#train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
#correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
#accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess.run(tf.initialize_all_variables())

#### SAVE MODEL
to_save = []
names = []
weights = locals().copy()
for var_name in weights:
    if var_name[-2:] == '_o':
        exec('to_save.append(' + var_name + '.eval())')
        names.append(var_name)
        
np.savez('saved_nets/weights_for_'+name, to_save, names)
