# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 15:40:02 2017

@author: user
"""
#from PIL import Image
import numpy as np

def createNeighbourSets(EMimage, synapses, n, steps = (1,1,1), nz = 0):
    """ Creates a numpy array of the neighbours in the (2*n+1)*(2*nz+1) grid around each pixel,
    moving in steps given by 'steps' """
    #EMimage and synapses should both be numpy arrays of same dimensions
    assert (EMimage.shape == synapses.shape)
    x_step, y_step, z_step = steps
    depth, height, width = EMimage.shape
    
    numDataPoints = int(np.ceil((height-2*n)/y_step)*np.ceil((width-2*n)/x_step)*np.ceil((depth-2*nz)/z_step))
    dimensions = (2*n+1)**2*(2*nz+1)
    
    #count the synapses
    numSynapses = 0
    for k in range(nz, depth-nz, z_step):
        for j in range(n, height-n, y_step):
            for i in range(n, width-n, x_step):
                if synapses[k][j][i]:
                    numSynapses += 1
    
    Synapses = np.zeros([numSynapses, dimensions])
    notSynapses = np.zeros([numDataPoints-numSynapses, dimensions])
    
    synapsesAdded = 0
    notSynapsesAdded = 0
    for k in range(nz, depth-nz, z_step):
        for j in range(n, height-n, y_step):
            for i in range(n, width-n, x_step):
                #compile a list of all the neighbours of the pixel
                neighbours = np.zeros([dimensions])
                neighboursAdded = 0
                for dk in range(-nz, nz+1):
                    for dj in range(-n, n+1):
                        for di in range(-n, n+1):
                            neighbours[neighboursAdded] = EMimage[k+dk][j+dj][i+di]
                            neighboursAdded += 1
                            
                if synapses[k][j][i]:
                    Synapses[synapsesAdded] = neighbours
                    synapsesAdded += 1
                else:
                    notSynapses[notSynapsesAdded] = neighbours
                    notSynapsesAdded += 1
    assert(synapsesAdded == numSynapses)
    assert(notSynapsesAdded == numDataPoints-numSynapses)
    
    return (Synapses, notSynapses)

""" some tests """
if __name__ == '__main__':
    em = readTif("C:/Users/user/Python/envs/tensorflow/train/image.tif")
    sy = readTif("C:/Users/user/Python/envs/tensorflow/train/synapse.tif")
    a = createNeighbourSets(em, sy, 3, steps = (50,50,20))
    del em, sy
