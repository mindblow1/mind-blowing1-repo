# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 22:05:40 2017

@author: user
"""
import numpy as np
import tensorflow as tf
from CNN import *

trainingStepSize = 0.001;

name = "gpu_25x25x3_rotation2"
n = 12 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 1#vertical length is 2n+1

global threeD 
threeD = nz>0


#def makeDoubleVector(y1):
#    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
#    y1 = tf.reshape(y1, [-1, 1])
#    yNot = tf.logical_not(tf.cast(y1, tf.bool))
#    yNot = tf.cast(yNot, tf.float32)
#    return tf.concat(1, [yNot, y1])
#
#def weight_variable(shape):
#  initial = tf.truncated_normal(shape, stddev=0.1)
#  return tf.Variable(initial)
#
#def bias_variable(shape):
#  initial = tf.constant(0.1, shape=shape)
#  return tf.Variable(initial)
#
#def conv(x, W):
#    if threeD:
#        return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='VALID')
#    else:
#        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')


sess = tf.InteractiveSession()

##### DEFINE CNN
if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)

#have 25x25x3

if threeD:
    W_conv0_o = weight_variable([2*nz+1, 4, 4, 1, 16])            #should really adjust size with n
    x_image = tf.reshape(x, [-1,2*nz+1, 2*n+1,2*n+1, 1])
#else:
#    W_conv0_o = weight_variable([4, 4, 1, 16])            #should really adjust size with n
#    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])
b_conv0_o = bias_variable([16])

h_conv0 = tf.nn.relu(conv(x_image, W_conv0_o, threeD) + b_conv0_o)  #22x22
h_conv0 = tf.reshape(h_conv0, [-1,22,22,16])
threeD = 0

W_conv1_o = weight_variable([4, 4, 16, 16])
b_conv1_o = bias_variable([16])
h_conv1 = tf.nn.relu(tf.nn.conv2d(h_conv0, W_conv1_o, strides = [1, 1, 1, 1], padding = 'SAME') + b_conv1_o)


#h_pooled0 = tf.nn.max_pool(h_conv0, [1,2,2,1], [1,2,2,1], 'VALID')                  #11x11
W_conv2_o = weight_variable([2, 2, 16, 16])
b_conv2_o = bias_variable([16])
h_pooled0 = tf.nn.relu(tf.nn.conv2d(h_conv1, W_conv2_o, strides = [1, 2, 2, 1], padding = 'VALID') + b_conv2_o)

if threeD:
    W_conv3_o = weight_variable([4, 4, 1, 16, 32])            #should really adjust size with n
    #x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1, 1])
else:
    W_conv3_o = weight_variable([4, 4, 16, 32])        #change the first 32    #should really adjust size with n
    #x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])    ##PRETTY SURE THIS IS UNNECESSARY
b_conv3_o = bias_variable([32])

h_conv3 = tf.nn.relu(conv(h_pooled0, W_conv3_o, threeD) + b_conv3_o) #8x8
###new layer above
W_fc1_o = weight_variable([8 * 8 * 32, 1024])
b_fc1_o = bias_variable([1024])

h_conv_flat = tf.reshape(h_conv3, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv_flat, W_fc1_o) + b_fc1_o)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2_o = weight_variable([1024, 64])
b_fc2_o = bias_variable([64])

h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o)

W_fc3_o = weight_variable([64, 2])
b_fc3_o = bias_variable([2])

h_fc3 = tf.nn.relu(tf.matmul(h_fc2, W_fc3_o) + b_fc3_o)

W_fc4_o = weight_variable([2, 2])
b_fc4_o = bias_variable([2])

y_conv = tf.nn.relu(tf.matmul(h_fc3, W_fc4_o) + b_fc4_o)

#probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
#probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
#train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
synapsesBits, not_synapseBits = tf.slice(correct_prediction, [0], [5]), tf.slice(correct_prediction, [5], [95])
TP = tf.reduce_sum(tf.cast(synapsesBits, tf.float32))
TN = tf.reduce_sum(tf.cast(not_synapseBits, tf.float32))
FP = 95-TN
FN = 5 - TN
prec = TP/(TP+FP)
rec = TP/(TP+FN)
F1 = 2*prec*rec/(prec+rec)
#tf.reduce_sum(tf.cast(correct, tf.float32))
#accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess.run(tf.initialize_all_variables())

#### SAVE MODEL
to_save = []
names = []
weights = locals().copy()
for var_name in weights:
    if var_name[-2:] == '_o':
        exec('to_save.append(' + var_name + '.eval())')
        names.append(var_name)
        
np.savez('/home/threeyp/mindblowing/saved_nets/weights_for_'+name, to_save, names)
#np.savez('C:/Users/user/Documents/3YP/cnn_files/weights_for_testo', to_save, names)
