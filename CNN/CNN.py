# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 17:03:15 2017

@author: user
"""

import dataInputNoPIL as dataInput
import numpy as np
import random
import tensorflow as tf

def makeDoubleVector(y1):
    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
    y1 = tf.reshape(y1, [-1, 1])
    yNot = tf.logical_not(tf.cast(y1, tf.bool))
    yNot = tf.cast(yNot, tf.float32)
    return tf.concat(1, [yNot, y1])

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

#def getPredictions(sess, xData, n, nz):
#    if nz>0:
#        xData = np.reshape(xData, [-1, (2*nz+1), (2*n+1)**2])
#    #note it doesn't matter what y1 is set to
#    ans = np.ndarray([xData.shape[0],1])
#    for MIN in range(0,xData.shape[0],100):
#        MAX = min(MIN+100, xData.shape[0])
#        result = sess.run([probSynapse], feed_dict={ x: xData[MIN:MAX], y1: [0]*100, keep_prob: 1.0})[0]
#        ans[MIN:MAX] = result
#    return ans

def conv(x, W, threeD):
    if threeD:
        return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='VALID')
    else:
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')
    
def calcF1(synapse_predictions, not_synapse_predictions, prob = 0.5, andMore = False):
    """synapse predictions is an array of probabilities that each real synapse is a syanpse
    not_synapse_predictions is for each non-synapse
    prob is the threshold probability to make the prediciton"""
    if len(synapse_predictions>0):
        TP = sum(synapse_predictions>prob)[0]
    #    if TP == 0:
    #        if andMore:
    #            return (0, 0, 0, 0)
    #        else:
    #            return 0        #F1 score will be zero and returning now prevents division by zero errors
        FN = sum(synapse_predictions<=prob)[0]
    else:
        TP, FN = 0, 0
    FP = sum(not_synapse_predictions>prob)[0]
    TN = sum(not_synapse_predictions<=prob)[0]
    if TP == 0:
        if andMore:
            return (0, TP, FN, FP, TN)
        else:
            return 0
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1 = 2*precision*recall/(precision+recall)
    if andMore:
        return (F1, TP, FN, FP, TN)
    else:
        return F1

def dataSample(not_synapseX, synapseX, numPoints, proportionSynapse, n, nz):
    #returns array of neighbours and y data for randomly selected points
    synSamples = int(numPoints*proportionSynapse)
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*nz+1)*(2*n+1)**2])
    for point in range(synSamples):
        data[point] = synapseX[synapseIndex[point]]
    for point in range(notSynSamples):
        data[synSamples+point] = not_synapseX[notSynapseIndex[point]]
    y = np.array([1]*synSamples+[0]*notSynSamples)
        
    if nz>0:
        data = np.reshape(data, [-1, (2*nz+1), (2*n+1)**2])
    return (data, y)

def dataSampleRotate3D(not_synapseX, synapseX, numPoints, proportionSynapse, n, nz):
    #returns array of neighbours and y data for randomly selected points
    synSamples = int(numPoints*proportionSynapse)
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*nz+1), (2*n+1), (2*n+1)])
    for point in range(synSamples):
        datum = synapseX[synapseIndex[point]]
        datum = np.reshape(datum, [(2*nz+1), (2*n+1), (2*n+1)])
        flips = np.random.choice(2,3)  #axes to flip about
        if flips[0]:
            datum = datum[::-1,:,:]
        if flips[1]:
            datum = datum[:,::-1,:]
        if flips[2]:
            datum = datum[:,:,::-1]
        rotations = np.random.choice(4) #number of multiples of 90 to flip the array
        for k in range(2*nz+1):
            datum[k] = np.rot90(datum[k], rotations)
        data[point] = datum
    for point in range(notSynSamples):
        datum = not_synapseX[notSynapseIndex[point]]
        data[synSamples+point] = np.reshape(datum, [(2*nz+1), (2*n+1), (2*n+1)])
    y = np.array([1]*synSamples+[0]*notSynSamples)
    return (data, y)

def makeDoubleVector(y1):
    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
    y1 = tf.reshape(y1, [-1, 1])
    yNot = tf.logical_not(tf.cast(y1, tf.bool))
    yNot = tf.cast(yNot, tf.float32)
    return tf.concat(1, [yNot, y1])
