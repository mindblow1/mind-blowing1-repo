function [ TP, FP, TN, FN ] = sos_evaluate_classification_metrics( ground, test )
% Pass in matrices of classifications to return TP, FP, TN and FN metrics.
% Arguements must be logicals or equal to 0 or 1.
% Arguement 'ground' is the ground truth classifications.
% Arguement 'test' is the matrix of predictions.
% Returns a 4-vector of true positive, false positive, true negative, and
%   false negative.

if ( (size(ground,1) ~= size(test,1)) && (size(ground,2) ~= size(test,2)) && (size(ground,3) ~= size(test,3)))
    error('Inputs must be of same size');
end

TP = 0;
FP = 0;
TN = 0;
FN = 0;

fprintf('Scaling test max from %4.4f to 1 \n',max(test(:)));
test = test / max(test(:));


fprintf('Scaling ground max from %4.4f to 1 \n',max(ground(:)));
ground = ground ./ max(ground(:));

    

sos_stack_viewer(ground);
sos_stack_viewer(test);


for i = 1:size(ground,1)
    
    for j = 1:size(ground,2)
        
        for k = 1:size(ground,3)
            
            if test(i,j,k) == 1
            
                if ground(i,j,k) == test(i,j,k)
                    TP = TP + 1;
                else
                    FP = FP + 1;
                end
                
            else
                
                if ground(i,j,k) == test(i,j,k)
                    TN = TN + 1;
                else
                    FN = FN + 1;
                end

            end
            
        end
    end
end





end

