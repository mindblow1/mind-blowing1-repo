function [Overlayed] = sos_overlay_logical_labels(image,labels)

Overlayed = zeros(size(image,1),size(image,2),3);

for i = 1:size(image,1)
   for j = 1:size(image,2)
       for k = 1:size(image,3)
           if (labels(i,j,k) ~= 0)
               Overlayed(i,j,k,:) = labels(i,j,k,:);
           else
               Overlayed(i,j,k,:) = image(i,j,k);
           end           
           
       end

   end
end

end