function [pixel_error] = sos_evaluate_pixel_error(ground_truth, test)
% Takes input of 3-D matrix of ground truth classifications and predicted
% Classifications
% Returns the fraction of pixels that have been correctly identified.

if (size(ground_truth,1) ~= size(test,1)) || (size(ground_truth,2) ~= size(test,2)) || (size(ground_truth,3) ~= size(test,3))
    error('Input matrices must be same size.')
end


pixel_error = sum(sum(sum(ground_truth == test)));

pixel_error = pixel_error/numel(ground_truth);




end