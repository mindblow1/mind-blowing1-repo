function [] = sos_save_data(stack, path)
% Saves the image stack as a multi-page TIFF file. 

% Inputs of the image stack as a double, and the relative file path to
% write the images to.

slide_count = size(stack,3);

output_name = strcat(path, '.tif');

t = Tiff(output_name,'w');

for k = 1:slide_count
    
    tagstruct.ImageLength = size(stack(:,:,k),1);
    tagstruct.ImageWidth = size(stack(:,:,k),2);
    tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
    tagstruct.BitsPerSample = 8;
    tagstruct.SamplesPerPixel = 1;
    tagstruct.RowsPerStrip = 16;
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
    tagstruct.Software = 'MATLAB';
    t.setTag(tagstruct)
   
    t.write(uint8(stack(:,:,k)));
    t.writeDirectory;
    
end

t.close();
end