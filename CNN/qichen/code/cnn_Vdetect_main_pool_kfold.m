function [yerror] = cnn_Vdetect_main_pool_kfold(u_par,varargin)
% CNN_MNIST  Demonstrated MatConNet on MNIST

yerror=0;

run('/opt/matconvnet-1.0-beta17/matlab/vl_setupnn.m') ;

opts.expDir = fullfile('data','mnist-baseline') ;
[opts, varargin] = vl_argparse(opts, varargin) ;

% opts.dataDir = 'data/mnist' ;
opts.imdbPath = fullfile(opts.expDir,u_par.dataset);  %Location for i/p dataz
opts.useBatchNorm = false ;
opts.networkType = 'simplenn' ; %CNN network type
opts.train = struct() ;
[opts, varargin] = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

%%% MNIST-SPECIFIC
if exist(opts.imdbPath, 'file')
    load(opts.imdbPath) ;
else
    error('Data File does not exist!!!');
end

%%% MNIST-SPECIFIC
net.meta.classes.name = arrayfun(@(x)sprintf('%d',x),1:u_par.numClasses,'UniformOutput',false) ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

switch opts.networkType
  case 'simplenn', trainfn = @cnn_slpdb_train ; % CHANGE XXXX
  case 'dagnn', trainfn = @cnn_train_dag ;
end
load('Vdetect_imdb_calculated')
opts.kfold=1;
for k = 1:10
    
    net = cnn_Vdetect_init_pool(u_par, 'useBatchNorm', opts.useBatchNorm, ...
                     'networkType', opts.networkType) ; %CHANGE XXXXX
    
    test = (imdb.meta.casekfold == k); train = ~test;
    imdb_train.images.data=imdb.images.data(:,:,:,train);
    imdb_train.images.labels=imdb.images.labels(train);
    imdb_train.images.data_mean=imdb.images.data_mean;
    imdb_train.images.set=imdb.images.set(train);

    [net, info] = trainfn(net, imdb_train, getBatch(opts),u_par, ...
      'expDir', opts.expDir, ...
      net.meta.trainOpts, ...
      opts.train, ...
      'val', find(imdb_train.images.set == 3),'kfold',k) ;
    k
    imdb_test.images.data=imdb.images.data(:,:,:,test);
    imdb_test.images.labels=imdb.images.labels(test);
    imdb_test.images.data_mean=imdb.images.data_mean;
    imdb_test.images.set=imdb.images.set(test);
    
    [acc1(k), acc2(k), acc3(k), C_A1(:,:,k), C_A2(:,:,k), C_A3(:,:,k)] = cnn_test(net,imdb_train,10);
  
    [acc21(k), acc22(k), acc23(k), C_A21(:,:,k), C_A22(:,:,k), C_A23(:,:,k)] = cnn_test(net,imdb_test,10);
    
    yerror=yerror+info.val.objective(end)
    
end
    macc1=mean(acc1)
    macc21=mean(acc21)
    yerror=yerror/5

%   [net, info] = trainfn(net, imdb, getBatch(opts),u_par, ...
%   'expDir', opts.expDir, ...
%   net.meta.trainOpts, ...
%   opts.train, ...
%   'val', find(imdb.images.set == 3)) ;

% cnn_test;


% --------------------------------------------------------------------
function fn = getBatch(opts)
% --------------------------------------------------------------------
switch lower(opts.networkType)
  case 'simplenn'
    fn = @(x,y) getSimpleNNBatch(x,y) ;
  case 'dagnn'
    bopts = struct('numGpus', numel(opts.train.gpus)) ;
    fn = @(x,y) getDagNNBatch(bopts,x,y) ;
end

% --------------------------------------------------------------------
function [images, labels] = getSimpleNNBatch(imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;

% --------------------------------------------------------------------
function inputs = getDagNNBatch(opts, imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;
if opts.numGpus > 0
  images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

% --------------------------------------------------------------------
% function imdb = getMnistImdb(opts)
% % --------------------------------------------------------------------
% % Preapre the imdb structure, returns image data with mean image subtracted
% files = {'train-images-idx3-ubyte', ...
%          'train-labels-idx1-ubyte', ...
%          't10k-images-idx3-ubyte', ...
%          't10k-labels-idx1-ubyte'} ;
% 
% if ~exist(opts.dataDir, 'dir')
%   mkdir(opts.dataDir) ;
% end
% 
% for i=1:4
%   if ~exist(fullfile(opts.dataDir, files{i}), 'file')
%     url = sprintf('http://yann.lecun.com/exdb/mnist/%s.gz',files{i}) ;
%     fprintf('downloading %s\n', url) ;
%     gunzip(url, opts.dataDir) ;
%   end
% end
% 
% f=fopen(fullfile(opts.dataDir, 'train-images-idx3-ubyte'),'r') ;
% x1=fread(f,inf,'uint8');
% fclose(f) ;
% x1=permute(reshape(x1(17:end),28,28,60e3),[2 1 3]) ;
% 
% f=fopen(fullfile(opts.dataDir, 't10k-images-idx3-ubyte'),'r') ;
% x2=fread(f,inf,'uint8');
% fclose(f) ;
% x2=permute(reshape(x2(17:end),28,28,10e3),[2 1 3]) ;
% 
% f=fopen(fullfile(opts.dataDir, 'train-labels-idx1-ubyte'),'r') ;
% y1=fread(f,inf,'uint8');
% fclose(f) ;
% y1=double(y1(9:end)')+1 ;
% 
% f=fopen(fullfile(opts.dataDir, 't10k-labels-idx1-ubyte'),'r') ;
% y2=fread(f,inf,'uint8');
% fclose(f) ;
% y2=double(y2(9:end)')+1 ;
% 
% set = [ones(1,numel(y1)) 3*ones(1,numel(y2))];
% data = single(reshape(cat(3, x1, x2),28,28,1,[]));
% dataMean = mean(data(:,:,:,set == 1), 4);
% data = bsxfun(@minus, data, dataMean) ;
% 
% imdb.images.data = data ;
% imdb.images.data_mean = dataMean;
% imdb.images.labels = cat(2, y1, y2) ;
% imdb.images.set = set ;
% imdb.meta.sets = {'train', 'val', 'test'} ;
% imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:9,'uniformoutput',false) ;