% % workbench3

% cnn_Vdetect_kfold_f(2,'PAUL');
% for i=1:25
% %     stat{i}=cnn_train_test(i,'PAUL',10)
%
%     A1{i}=sum(stat{i}.C_A1,3);
%     B1(i)=mean(stat{i}.acc1);
%     C1(i)=mean(stat{i}.Se1);
%     D1(i)=mean(stat{i}.Sp1);
%     E1(i)=mean(stat{i}.F11);
%
%     A2{i}=sum(stat{i}.C_A2,3);
%     B2(i)=mean(stat{i}.acc2);
%     C2(i)=mean(stat{i}.Se2);
%     D2(i)=mean(stat{i}.Sp2);
%     E2(i)=mean(stat{i}.F12);
%
% 	  A3{i}=sum(stat{i}.C_A3,3);
%     B3(i)=mean(stat{i}.acc3);
%     C3(i)=mean(stat{i}.Se3);
%     D3(i)=mean(stat{i}.Sp3);
%     E3(i)=mean(stat{i}.F13);
% end
% save(['mitdb/stat_25_valid'],'stat');

%
% for i=1:2
%    examplecell{i} = examplefunc();
% end

% for i=1:11
%     i
%     [acc1(i,:), Se(i,:), Sp(i,:), F1(i,:)]=cnn_test_test(i,'PAUL',10);
% end
% save
% save(['mitdb/stat_21_valid']);




fileno=[100,101,102,103,104,105,106,107,108,109,111,112,...
        113,114,115,116,117,118,119,121,122,123,124,200,...
        201,202,203,205,207,208,209,210,212,213,214,215,...
        217,219,220,221,222,223,228,230,231,232,233,234];
    
for ii=1:3
    switch ii
        case 1
            wavetype='PAUL';
        case 2
            wavetype='MORLET';
        case 3
            wavetype='DOG';
    end

            for i=1:12
                load(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat.mat'])
                A1(i)=mean(stat.acc1);
                B1(i)=mean(stat.F11);
                C1(i)=mean(stat.Se1);
                D1(i)=mean(stat.Sp1);
                Ae1(i)=std(stat.acc1);
                Be1(i)=std(stat.F11);
                Ce1(i)=std(stat.Se1);
                De1(i)=std(stat.Sp1);
                
                A2(i)=mean(stat.acc2);
                B2(i)=mean(stat.F12);
                C2(i)=mean(stat.Se2);
                D2(i)=mean(stat.Sp2);
                Ae2(i)=std(stat.acc2);
                Be2(i)=std(stat.F12);
                Ce2(i)=std(stat.Se2);
                De2(i)=std(stat.Sp2);
                
%                 for iii=1:48
%                     fn(iii)=sum(stat.er_fn==iii);
%                     fp(iii)=sum(stat.er_fp==iii);
%                 end
%                 figure
%                 pareto(fn)
%                 tt_fn=sprintf('%s %d fn',wavetype,i*5);
%                 title(tt_fn)
%                 
%                 figure
%                 pareto(fp)
%                 tt_fp=sprintf('%s %d fp',wavetype,i*5);
%                 title(tt_fp)
%                 
%                 fprintf('wavetype: %s %d\n',wavetype,i*5);
%                 fprintf('fn\n');
%                 tabulate(stat.er_fn)
%                 fprintf('fp\n');
%                 tabulate(stat.er_fp)
                
                %                 A3(i)=mean(stat.acc3);
                %                 B3(i)=mean(stat.F13);
                %                 C3(i)=mean(stat.Se3);
                %                 D3(i)=mean(stat.Sp3);
                %                 Ae3(i)=std(stat.acc3);
                %                 Be3(i)=std(stat.F13);
                %                 Ce3(i)=std(stat.Se3);
                %                 De3(i)=std(stat.Sp3);
            end

            figure
            hold on
            plot(A1,'b')
            plot(B1,'r')
            plot(C1,'g')
            plot(D1,'k')
            errorbar(1:12,A1,Ae1,'b')
            errorbar(1:12,B1,Be1,'r')
            errorbar(1:12,C1,Ce1,'g')
            errorbar(1:12,D1,De1,'k')
            title([wavetype,' test']);

            figure
            hold on
            plot(A2,'b')
            plot(B2,'r')
            plot(C2,'g')
            plot(D2,'k')
            errorbar(1:12,A2,Ae2,'b')
            errorbar(1:12,B2,Be2,'r')
            errorbar(1:12,C2,Ce2,'g')
            errorbar(1:12,D2,De2,'k')
            title([wavetype,' training']);

%             figure
%             hold on
%             plot(A3,'b')
%             plot(B3,'r')
%             plot(C3,'g')
%             plot(D3,'k')
%             errorbar(1:12,A3,Ae3,'b')
%             errorbar(1:12,B3,Be3,'r')
%             errorbar(1:12,C3,Ce3,'g')
%             errorbar(1:12,D3,De3,'k')
%             title([wavetype,' validation']);

end




% wavetype='PAUL';
% 
% for i=1:12
%     load(['Vdetect_imdb_output_',wavetype,'_25_sec_stat_',num2str(i+9),'.mat'])
%     A1(i)=mean(stat.acc1);
%     B1(i)=mean(stat.F11);
%     C1(i)=mean(stat.Se1);
%     D1(i)=mean(stat.Sp1);
%     Ae1(i)=std(stat.acc1);
%     Be1(i)=std(stat.F11);
%     Ce1(i)=std(stat.Se1);
%     De1(i)=std(stat.Sp1);
%     
%     A2(i)=mean(stat.acc2);
%     B2(i)=mean(stat.F12);
%     C2(i)=mean(stat.Se2);
%     D2(i)=mean(stat.Sp2);
%     Ae2(i)=std(stat.acc2);
%     Be2(i)=std(stat.F12);
%     Ce2(i)=std(stat.Se2);
%     De2(i)=std(stat.Sp2);
% end
% 
% figure
% hold on
% plot(A1,'b')
% plot(B1,'r')
% plot(C1,'g')
% plot(D1,'k')
% errorbar(1:12,A1,Ae1,'b')
% errorbar(1:12,B1,Be1,'r')
% errorbar(1:12,C1,Ce1,'g')
% errorbar(1:12,D1,De1,'k')
% title([wavetype,' test']);
% 
% figure
% hold on
% plot(A2,'b')
% plot(B2,'r')
% plot(C2,'g')
% plot(D2,'k')
% errorbar(1:12,A2,Ae2,'b')
% errorbar(1:12,B2,Be2,'r')
% errorbar(1:12,C2,Ce2,'g')
% errorbar(1:12,D2,De2,'k')
% title([wavetype,' training']);
