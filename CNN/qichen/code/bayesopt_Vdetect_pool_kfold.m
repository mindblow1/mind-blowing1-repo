clear all, close all
addpath(genpath('/opt/bayesopt-master/matlab'))
global iter_val numL inputS
iter_val=1;

diary('Vdetect_output_100.txt');
diary on;

params.n_iterations = 100; %Total number of function evaluations
params.n_init_samples = 10;
params.crit_name = 'cEI';  %Expected Improvement for criteria
params.surr_name = 'sGaussianProcess'; %Surrogate model
params.noise = 1e-6;
params.kernel_name = 'kMaternARD5'; %Kernal for computing Covariance
params.kernel_hp_mean = [1];
params.kernel_hp_std = [10];
params.verbose_level = 1;
params.log_filename = 'matbopt.log';

disp('Continuous optimization');

numL=3; % number of conv layers
inputS=[36 45 1];

[k_lb_r,k_ub_r,ip_min_r]=bounds_cnn_pool(numL,inputS(1),3);
[k_lb_c,k_ub_c,ip_min_c]=bounds_cnn_pool(numL,inputS(2),1);

numFilt_lb=[5,15,100];
numFilt_ub=[60,120,600];

fun = 'cnn_Vdetect_fun_pool_kfold'; n = 3+2*(numL-1)+numel(numFilt_lb);

lb = [0;0;10;k_lb_r(1:end-1)';k_lb_c(1:end-1)';numFilt_lb'];
ub = [0.1;0.1;25;k_ub_r(1:end-1)';k_ub_c(1:end-1)';numFilt_ub'];

tic;
format long
bayesoptcont(fun,n,params,lb,ub)
toc;

diary off