function y = cnn_Vdetect_fun_pool_kfold(x)
% Function that needs to be passed into the bopt program
% LR, WD, Epoch, Filt_r, Filt_c, NumFilt
tic
global iter_val numL inputS data_base batch_size
fprintf('Iteration: %d\n',iter_val);
iter_val=iter_val+1;

count=4;

par.inputSize=inputS;
par.numLayers=numL;

if par.numLayers~=1
    [k_lb_r,k_ub_r,par.ip_min_r]=bounds_cnn_pool(par.numLayers,par.inputSize(1),3);
    [k_lb_c,k_ub_c,par.ip_min_c]=bounds_cnn_pool(par.numLayers,par.inputSize(2),1);
end

par.LR=x(1);
par.weightDecay=x(2);
par.numEpochs=round(x(3));
par.numBatches=batch_size;
par.dataset=data_base;
par.numClasses=2;
par.plot=0;

if par.numLayers==1
    par.ksize_r=[round(x(count)),0];
    count= count+par.numLayers;
    par.ksize_c=[round(x(count)),0];
    count= count+par.numLayers;
    par.numFilt=round(x(count:end));
else
    par.ksize_r=[round(x(count:count+par.numLayers-2)),0];
    count= count+par.numLayers-1;
    par.ksize_c=[round(x(count:count+par.numLayers-2)),0];
    count= count+par.numLayers-1;
    par.numFilt=round(x(count:end));    
end

% par.ksize_r=[round(x(4)),round(x(5)),round(x(6)),0];
% par.ksize_c=[round(x(7)),round(x(8)),round(x(9)),0];

fprintf('Layers: %d\n',par.numLayers); %, LR: %f, WD: %f, numEpochs: %d \n',par.numLayers,par.LR,par.weightDecay,par.numEpochs);

format long
disp(x);
% [net_fc, info_fc] = cnn_mimic_main(par,'expDir', 'data_mnist/mnist-baseline', 'useBatchNorm', false);
[y] = cnn_Vdetect_main_pool_kfold(par,'expDir', 'Vdetect_imdb_calculated.mat', 'useBatchNorm', false);

% y=info_fc.val.objective(end)

if isnan(y)
	y=100000;
end
if isinf(y)
	y=100000;
end

fprintf('Validation Cost: %f\n',y)
toc

end