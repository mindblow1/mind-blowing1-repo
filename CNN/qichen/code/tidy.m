for ii=1:3
    switch ii
        case 1
            wavetype='PAUL';
        case 2
            wavetype='MORLET';
        case 3
            wavetype='DOG';
    end
    
    for i=1:12
        load(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat_all.mat'])
        stat2=stat;
        load(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat.mat'])
        stat.acc2=stat2.acc2;
        stat.tp2=stat2.tp2;
        stat.fp2=stat2.fp2;
        stat.tn2=stat2.tn2;
        stat.fn2=stat2.fn2;
        stat.Se2=stat2.Se2;
        stat.Sp2=stat2.Sp2;
        stat.F12=stat2.F12;
        stat.C_A2=stat2.C_A2;
        clear stat.acc3 stat.tp3 stat.fp3 stat.tn3 stat.fn3 stat.Se3 stat.Sp3 stat.F13 stat.C_A3;
        save(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat.mat'],'stat')
    end
end