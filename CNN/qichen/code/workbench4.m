% workbench 4

V_all=[];
nonV_all=[];
nth_win=1;

% vector consisting of the filenames
fileno=[100,101,102,103,104,105,106,107,108,109,111,112,...
    113,114,115,116,117,118,119,121,122,123,124,200,...
    201,202,203,205,207,208,209,210,212,213,214,215,...
    217,219,220,221,222,223,228,230,231,232,233,234];

timeinsec=1;

for nth_file=16:48
    
    V=0;
    nonV=0;
    
    % call rdsamp to read in the signal data
    [tm, signal,Fs]=rdsamp(['mitdb/',num2str(fileno(nth_file))]);
    % call rdann to read in the annotations
    [ann,type,subtype,chan,num,comments]=rdann(['mitdb/',num2str(fileno(nth_file))],'atr');
    
    % plot(signal(:,1))
    % hold on
    
    % exclude annotations that are not beats (e.g. noise)
    for i=1:length(ann)
        switch type(i)
            case 'N'
            case 'L'
            case 'R'
            case 'B'
            case 'A'
            case 'a'
            case 'J'
            case 'S'
            case 'V'
            case 'r'
            case 'F'
            case 'e'
            case 'j'
            case 'n'
            case 'E'
            case '/'
            case 'f'
            case 'Q'
            case '?'
            otherwise
                % set these annotations to 1
                ann(i)=1;
                type(i)='1';
        end
    end
    
    % introducing a Hanning window
    Hw=hanning(360*timeinsec);
    
    % number of windows stored
    nth_win;
    
    % count the number of V and nonV
    for i=1:length(ann)
        switch type(i)
            case 'V'
                % select a timeinsec second window around the annotated beat
                % using findbeat function
                if(ann(i)<3*360)
                elseif(ann(end)-(ann(i))<3*360)
                else
                    V=V+1;
                    beat=findbeat(timeinsec,i,signal,ann);
                    % remove baseline elevation
                    beat=detrend(beat).*Hw;
                    
                    subplot(4,2,1)
                    plot(beat)
                    axis([0 360 -2 4])
                    ylabel('mV')
                    grid on
                    % plot the window ecg signal
                    % wavelet transform
                    
                    beat=boxpdf(beat);
                    wavetype='MORLET';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,3)
                    imagesc(beat_wt)
                    ylabel('Scaling Factor')
                    wavetype='PAUL';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,5)
                    imagesc(beat_wt)
                    ylabel('Scaling Factor')
                    wavetype='DOG';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,7)
                    imagesc(beat_wt)
                    xlabel('Data Points')
                    ylabel('Scaling Factor')
                    drawnow
     
                    beat_wt=resample(beat_wt',45,360*timeinsec)';
                    imdb_unb.images.data(:,:,1,nth_win)=beat_wt;
                    imdb_unb.images.labels(nth_win)=2;  % 2 for V
                    imdb_unb.meta.casen(nth_win)=nth_file;
                    nth_win=nth_win+1;
                end
            case '1'
            otherwise
                % select a timeinsec second window around the annotated beat
                % using findbeat function
                if(ann(i)<3*360)
                elseif(ann(end)-(ann(i))<3*360)
                else
                    nonV=nonV+1;
                    beat=findbeat(timeinsec,i,signal,ann);
                    % remove baseline elevation
                    beat=detrend(beat).*Hw;
                    subplot(4,2,2)
                    plot(beat)
                    axis([0 360 -2 4])
                    ylabel('mV')
                    grid on
                    % plot the window ecg signal
                    % wavelet transform
                    beat=boxpdf(beat);
                    wavetype='MORLET';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,4)
                    imagesc(beat_wt)
                    ylabel('Scaling Factor')
                    wavetype='PAUL';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,6)
                    imagesc(beat_wt)
                    ylabel('Scaling Factor')
                    wavetype='DOG';
                    beat_wt=wt(beat,wavetype);
                    subplot(4,2,8)
                    imagesc(beat_wt)
                    xlabel('Data Points')
                    ylabel('Scaling Factor')
                    
                    colorbar
                    drawnow

                    beat_wt=resample(beat_wt',45,360*timeinsec)';
                    imdb_unb.images.data(:,:,1,nth_win)=beat_wt;
                    imdb_unb.images.labels(nth_win)=1;  % 1 for nonV
                    imdb_unb.meta.casen(nth_win)=nth_file;
                    nth_win=nth_win+1;
                end
        end
    end
    
    nth_win
    
    V_all(nth_file)=V;
    nonV_all(nth_file)=nonV;

    clear ann chan comments num signal subtype tm type
    
end
