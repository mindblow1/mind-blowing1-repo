function [] = cnn_train_test(split)
% function [acc3, C_A3] = cnn_test(net,imdb,split,imdb_train)
% test the CNN model

% % load(['mitdb/Vdetect_imdb_',wavetype,'_',num2str(timeinsec),'_sec_calculated.mat']);imdb_unb=imdb;
% load(['mitdb/Vdetect_imdb_output_',wavetype,'_',num2str(timeinsec*10),'_sec_net_',num2str(iter),'.mat']);

load(['imdb_output_balance_1_net.mat']);

load(['imdb_5_unb.mat']);
% imdb_unb=imdb;
imdb_unb.images.data=single(imdb_unb.images.data);

% load(['Vdetect_imdb_output_',wavetype,'_',num2str(timeinsec*10),'_sec_net.mat']);
% load(['Vdetect_imdb_',wavetype,'_',num2str(timeinsec*10),'_sec_unb_calculated.mat']);

stat.er_fn=[];
stat.er_fp=[];
stat.er_tn=[];
stat.er_tp=[];

% for k = 1:10  % k-fold
k=1;
net = net_total;
%     load(['net-epoch-',num2str(k),'-',num2str(iter),'.mat']);
%
test = logical(ones(1,length(imdb_unb.images.data)));
imdb_test.images.data=imdb_unb.images.data(:,:,:,test);
imdb_test.images.labels=imdb_unb.images.labels(test);
imdb_test.images.data_mean=imdb_unb.images.data_mean;
imdb_test.images.set=imdb_unb.images.set(test);
%
%     train = ~(imdb.meta.casekfold==k);
% 	  training=train&(imdb.images.set==1);
%     imdb_train.images.data=imdb.images.data(:,:,:,training);
%     imdb_train.images.labels=imdb.images.labels(training);
%     imdb_train.images.data_mean=imdb.images.data_mean;
%     imdb_train.images.set=imdb.images.set(training);
%
%     validation=train&(imdb.images.set==3);
%     imdb_valid.images.data=imdb.images.data(:,:,:,validation);
%     imdb_valid.images.labels=imdb.images.labels(validation);
%     imdb_valid.images.data_mean=imdb.images.data_mean;
%     imdb_valid.images.set=imdb.images.set(validation);

train = (imdb_unb.images.set==3);
training=train;
imdb_train.images.data=imdb_unb.images.data(:,:,:,training);
imdb_train.images.labels=imdb_unb.images.labels(training);
imdb_train.images.data_mean=imdb_unb.images.data_mean;
imdb_train.images.set=imdb_unb.images.set(training);

if nargin<1
    split=1;
end

%run('/opt/matconvnet-1.0-beta20/matlab/vl_setupnn.m');
net.layers{1,end}.type='softmax';
if split==1
    res=vl_simplenn(net,imdb_test.images.data);
    for i=1:max(imdb_test.images.labels)
        for j=1:size(res(end).x,4)
            ret(i,j)=res(end).x(1,1,i,j);
        end
    end
    [m im]=max(ret);
    type=imdb_test.images.labels;
    type=double(type);
    stat.C_A1(:,:,k)=confusionmat(im,type,'order',[max(imdb_test.images.labels):-1:1]);
    
    
    rrr=type-im;
    casen=imdb_unb.meta.casen(test);
    stat.type{k}=type;
    stat.result{k}=im;
    stat.er_fn=[stat.er_fn casen(find(rrr==1))];
    stat.er_fp=[stat.er_fp casen(find(rrr==-1))];
    stat.er_tn=[stat.er_tn casen(find(rrr==0 & type==1))];
    stat.er_tp=[stat.er_tp casen(find(rrr==0 & type==2))];
    
    
    res=vl_simplenn(net,imdb_train.images.data);
    for i=1:2
        for j=1:size(res(end).x,4)
            ret(i,j)=res(end).x(1,1,i,j);
        end
    end
    [m im]=max(ret);
    type=imdb_train.images.labels;
    type=double(type);
    stat.C_A2(:,:,k)=confusionmat(im,type,'order',[2:-1:1]);
    
    
    
    
    %         res=vl_simplenn(net,imdb_valid.images.data);
    %         for i=1:max(imdb_valid.images.labels)
    %             for j=1:size(res(end).x,4)
    %                 ret(i,j)=res(end).x(1,1,i,j);
    %             end
    %         end
    %         [m im]=max(ret);
    %         type=imdb_valid.images.labels;
    %         stat.C_A3(:,:,k)=confusionmat(im,type,'order',[max(imdb_valid.images.labels):-1:1]);
    
    
else
    type=[];
    result=[];
    for kk=1:split
        each=ceil(size(imdb_test.images.data,4)/split);
        res=vl_simplenn(net,imdb_test.images.data(:,:,:,(kk-1)*each+1:min(kk*each,size(imdb_test.images.data,4))));
        ret=[];
        for i=1:2 % max(imdb_test.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type0=imdb_test.images.labels((kk-1)*each+1:min(kk*each,size(imdb_test.images.data,4)));
        type=[type type0];
        if ~isempty(im)
            result=[result im];
        end
    end
    type=double(type);
    stat.C_A1(:,:,k)=confusionmat(result,type,'order',[2:-1:1]);
    
%     rrr=type-result;
%     casen=imdb_unb.meta.casen(test);
%     stat.type{k}=type;
%     stat.result{k}=result;
%     stat.er_fn=[stat.er_fn casen(find(rrr==1))];
%     stat.er_fp=[stat.er_fp casen(find(rrr==-1))];
%     stat.er_tn=[stat.er_tn casen(find(rrr==0 & type==1))];
%     stat.er_tp=[stat.er_tp casen(find(rrr==0 & type==2))];
    
    
    type=[];
    result=[];
    for kk=1:split
        each=ceil(size(imdb_train.images.data,4)/split);
        res=vl_simplenn(net,imdb_train.images.data(:,:,:,(kk-1)*each+1:min(kk*each,size(imdb_train.images.data,4))));
        ret=[];
        for i=1:2 % max(imdb_train.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type0=imdb_train.images.labels((kk-1)*each+1:min(kk*each,size(imdb_train.images.data,4)));
        type=[type type0];
        if ~isempty(im)
            result=[result im];
        end
    end
    type=double(type);
    stat.C_A2(:,:,k)=confusionmat(result,type,'order',[2:-1:1]);
    
    %         type=[];
    %         result=[];
    %         for kk=1:split
    %             each=ceil(size(imdb_valid.images.data,4)/split);
    %             res=vl_simplenn(net,imdb_valid.images.data(:,:,:,(kk-1)*each+1:min(kk*each,size(imdb_valid.images.data,4))));
    %             ret=[];
    %             for i=1:max(imdb_valid.images.labels)
    %                 for j=1:size(res(end).x,4)
    %                     ret(i,j)=res(end).x(1,1,i,j);
    %                 end
    %             end
    %             [m im]=max(ret);
    %             type0=imdb_valid.images.labels((kk-1)*each+1:min(kk*each,size(imdb_valid.images.data,4)));
    %             type=[type type0];
    %             if ~isempty(im)
    %                 result=[result im];
    %             end
    %         end
    %         stat.C_A3(:,:,k)=confusionmat(result,type,'order',[max(imdb_valid.images.labels):-1:1]);
end
% end
% for k=1:10
    stat.tp1(k)=stat.C_A1(1,1,k);
    stat.fp1(k)=stat.C_A1(1,2,k);
    stat.fn1(k)=stat.C_A1(2,1,k);
    stat.tn1(k)=stat.C_A1(2,2,k);
    stat.Se1(k)=stat.tp1(k)/(stat.tp1(k)+stat.fn1(k));
    stat.Sp1(k)=stat.tn1(k)/(stat.tn1(k)+stat.fp1(k));
    stat.acc1(k)=(stat.tp1(k)+stat.tn1(k))/sum(sum(stat.C_A1(:,:,k)));
    stat.p1(k)=stat.tp1(k)/(stat.tp1(k)+stat.fp1(k));
    stat.Macc1(k)=2*stat.Se1(k)*stat.Sp1(k)/(stat.Se1(k)+stat.Sp1(k));
    stat.F11(k)=2*stat.Se1(k)*stat.p1(k)/(stat.Se1(k)+stat.p1(k));
    
    stat.tp2(k)=stat.C_A2(1,1,k);
    stat.fp2(k)=stat.C_A2(1,2,k);
    stat.fn2(k)=stat.C_A2(2,1,k);
    stat.tn2(k)=stat.C_A2(2,2,k);
    stat.Se2(k)=stat.tp2(k)/(stat.tp2(k)+stat.fn2(k));
    stat.Sp2(k)=stat.tn2(k)/(stat.tn2(k)+stat.fp2(k));
    stat.acc2(k)=(stat.tp2(k)+stat.tn2(k))/sum(sum(stat.C_A2(:,:,k)));
    stat.p2(k)=stat.tp2(k)/(stat.tp2(k)+stat.fp2(k));
    stat.Macc2(k)=2*stat.Se2(k)*stat.Sp2(k)/(stat.Se2(k)+stat.Sp2(k));
    stat.F12(k)=2*stat.Se2(k)*stat.p2(k)/(stat.Se2(k)+stat.p2(k));
    
    %        stat.tp3(k)=stat.C_A3(1,1,k);
    %        stat.fp3(k)=stat.C_A3(1,2,k);
    %        stat.fn3(k)=stat.C_A3(2,1,k);
    %        stat.tn3(k)=stat.C_A3(2,2,k);
    %        stat.Se3(k)=stat.tp3(k)/(stat.tp3(k)+stat.fn3(k));
    %        stat.Sp3(k)=stat.tn3(k)/(stat.tn3(k)+stat.fp3(k));
    %        stat.acc3(k)=(stat.tp3(k)+stat.tn3(k))/sum(sum(stat.C_A3(:,:,k)));
    %        stat.F13(k)=2*stat.Se3(k)*stat.Sp3(k)/(stat.Se3(k)+stat.Sp3(k));
% end


% save(['mitdb/Vdetect_imdb_output_',wavetype,'_',num2str(timeinsec*10),'_sec_stat_',num2str(iter),'.mat'],'stat');
save(['imdb_output_balance_1_stat.mat'],'stat');

clear

end
