function [acc1, Se, Sp, F1] = cnn_test_test(timeinsec,wavetype,split,imdb_train)
% function [acc3, C_A3] = cnn_test(net,imdb,split,imdb_train)
% test the CNN model

load(['mitdb/Vdetect_imdb_output_',wavetype,'_',num2str(2),'_sec_net_',num2str(timeinsec),'.mat']);
load(['mitdb/Vdetect_imdb_',wavetype,'_',num2str(2),'_sec_unb_calculated.mat']);
% imdb_unb=imdb;
% load(['mitdb/Vdetect_imdb_',wavetype,'_',num2str(2),'_sec_unb_calculated.mat']);

for k = 1:10  % k-fold
    k
    net = net_total{k};
%     load(['net-epoch-',num2str(k),'-',num2str(timeinsec),'.mat']);
    test = (imdb_unb.meta.casekfold==k);
    imdb_test.images.data=imdb_unb.images.data(:,:,:,test);
    imdb_test.images.labels=imdb_unb.images.labels(test);
    imdb_test.images.data_mean=imdb_unb.images.data_mean;
    imdb_test.images.set=imdb_unb.images.set(test);
    if nargin==4 % adjust mean of imdb by imdb_train
        for kk=1:size(imdb_test.images.data,4)
            imdb_test.images.data(:,:,1,kk)=imdb_test.images.data(:,:,1,kk)+imdb_test.images.data_mean-imdb_train.images.data_mean;
        end
    end
    
    if nargin<3
        split=1;
    end
    
    %run('/opt/matconvnet-1.0-beta17/matlab/vl_setupnn.m');
    net.layers{1,end}.type='softmax';
    if split==1
        res=vl_simplenn(net,imdb_test.images.data);
        for i=1:max(imdb_test.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type=imdb_test.images.labels;
        C_A1(:,:,k)=confusionmat(im,type,'order',[max(imdb_test.images.labels):-1:1])
        
        
    else
        type=[];
        result=[];
        for kk=1:split
            each=ceil(size(imdb_test.images.data,4)/split);
            res=vl_simplenn(net,imdb_test.images.data(:,:,:,(kk-1)*each+1:min(kk*each,size(imdb_test.images.data,4))));
            ret=[];
            for i=1:max(imdb_test.images.labels)
                for j=1:size(res(end).x,4)
                    ret(i,j)=res(end).x(1,1,i,j);
                end
            end
            [m im]=max(ret);
            type0=imdb_test.images.labels((kk-1)*each+1:min(kk*each,size(imdb_test.images.data,4)));
            type=[type type0];
            if ~isempty(im)
                result=[result im];
            end
        end
        C_A1(:,:,k)=confusionmat(result,type,'order',[max(imdb_test.images.labels):-1:1]);      
    end
end
    for k=1:10
       tp(k)=C_A1(1,1,k);
       fp(k)=C_A1(1,2,k);
       fn(k)=C_A1(2,1,k);
       tn(k)=C_A1(2,2,k);
       Se(k)=tp(k)/(tp(k)+fn(k));
       Sp(k)=tn(k)/(tn(k)+fp(k));
       acc1(k)=(tp(k)+tn(k))/sum(sum(C_A1(:,:,k)));
       F1(k)=2*Se(k)*Sp(k)/(Se(k)+Sp(k));
    end
end
