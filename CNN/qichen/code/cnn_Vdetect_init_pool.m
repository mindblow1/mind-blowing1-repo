function net = cnn_Vdetect_init_pool(u_par,varargin)
% Function to initialize parameters of CNN. 
% Number of NN layers: Variable
% Kernel size of each layer: Variable
% The structure of layer: u_par.numLayers (CONV Layers) + 
%                         u_par.numLayers - 1 (pool Layers) + RELU + 
%                         CONV (Fully Connected Layer) + Softmax LogLoss

opts.useBatchNorm = false ;
opts.networkType = 'simplenn' ;
opts = vl_argparse(opts, varargin) ;
numFilt=u_par.numFilt;

%Extract the information about kernel size (Rows and Columns) and Number of
%Layers

n=u_par.numLayers;
ksize_r=u_par.ksize_r;
ksize_c=u_par.ksize_c;

ip_size_r=zeros(1,n);
ip_size_c=zeros(1,n);

ip_size_r(1)=u_par.inputSize(1);
ip_size_c(1)=u_par.inputSize(2);

% Update the Kernel Size depending on whether the user provided kernel
% sizes result in a valid output or not
for i=2:n-1
    if mod(ip_size_r(i-1)-ksize_r(i-1)+1,2)
        ksize_r(i-1)=ksize_r(i-1)+1;
    end
    if mod(ip_size_c(i-1)-ksize_c(i-1)+1,2)
        ksize_c(i-1)=ksize_c(i-1)+1;
    end
    ip_size_r(i)=(ip_size_r(i-1)-ksize_r(i-1)+1)/2;
    ip_size_c(i)=(ip_size_c(i-1)-ksize_c(i-1)+1)/2;
    if (ip_size_r(i)-ksize_r(i)+1<u_par.ip_min_r(i+1))  % Check if the convolution of i/p with kernel results in o/p lower than required size
        ksize_r(i)=ip_size_r(i)-u_par.ip_min_r(i+1)+1;
    end
    if (ip_size_c(i)-ksize_c(i)+1<u_par.ip_min_c(i+1))  % Check if the convolution of i/p with kernel results in o/p lower than required size
        ksize_c(i)=ip_size_c(i)-u_par.ip_min_c(i+1)+1;
    end
end

%Compute the size of o/p from Final layer
if mod(ip_size_r(n-1)-ksize_r(n-1)+1,2)
    ksize_r(n-1)=ksize_r(n-1)+1;
end
if mod(ip_size_c(n-1)-ksize_c(n-1)+1,2)
    ksize_c(n-1)=ksize_c(n-1)+1;
end
ksize_r(n)=(ip_size_r(n-1)-ksize_r(n-1)+1)/2; 
ksize_c(n)=(ip_size_c(n-1)-ksize_c(n-1)+1)/2;

rng('default');
rng(0) ;
f=1/100 ;

net.layers={};

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 8], ...
                           'stride', [2 8], ...
                           'pad', 0) ;

% Initialize the NN Layers
for i=1:n
    if i==1
        net.layers{end+1}=struct('type','conv', ...
                             'weights',{{f*randn(ksize_r(i),ksize_c(i),1,numFilt(i), 'single'), zeros(1, numFilt(i), 'single')}}, ...
                             'stride', 1, ...
                             'pad', 0);
    else
        net.layers{end+1}=struct('type','conv', ...
                             'weights',{{f*randn(ksize_r(i),ksize_c(i),numFilt(i-1),numFilt(i), 'single'), zeros(1, numFilt(i), 'single')}}, ...
                             'stride', 1, ...
                             'pad', 0);
    end
    if i<n
        net.layers{end+1} = struct('type', 'pool', ...
                                   'method', 'max', ...
                                   'pool', [2 2], ...
                                   'stride', 2, ...
                                   'pad', 0) ;
    end
        
end

% Initialize the Final 3 layers
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f*randn(1,1,numFilt(end),u_par.numClasses, 'single'), zeros(1,u_par.numClasses,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'softmaxloss') ;


if opts.useBatchNorm
  net = insertBnorm(net, 1) ;
  net = insertBnorm(net, 4) ;
  net = insertBnorm(net, 7) ;
end

% Meta parameters
net.meta.inputSize = u_par.inputSize;
net.meta.trainOpts.numClasses=u_par.numClasses;
net.meta.trainOpts.learningRate = u_par.LR ; %CHANGE XXXXX
net.meta.trainOpts.numEpochs = u_par.numEpochs ; %CHANGE XXXXX
net.meta.trainOpts.batchSize = 100; %u_par.numBatches ;

% Fill in defaul values
net = vl_simplenn_tidy(net) ;

% Switch to DagNN if requested
switch lower(opts.networkType)
  case 'simplenn'
    % done
  case 'dagnn'
    net = dagnn.DagNN.fromSimpleNN(net, 'canonicalNames', true) ;
    net.addLayer('error', dagnn.Loss('loss', 'classerror'), ...
             {'prediction','label'}, 'error') ;
  otherwise
    assert(false) ;
end

% --------------------------------------------------------------------
function net = insertBnorm(net, l)
% --------------------------------------------------------------------
assert(isfield(net.layers{l}, 'weights'));
ndim = size(net.layers{l}.weights{1}, 4);
layer = struct('type', 'bnorm', ...
               'weights', {{ones(ndim, 1, 'single'), zeros(ndim, 1, 'single')}}, ...
               'learningRate', [1 1 0.05], ...
               'weightDecay', [0 0]) ;
net.layers{l}.biases = [] ;
net.layers = horzcat(net.layers(1:l), layer, net.layers(l+1:end)) ;
end

end