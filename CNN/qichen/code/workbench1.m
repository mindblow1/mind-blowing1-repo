% % workbench
load('sample_beat');

wavetype='MORLET';

beat_2sec=[beat_1sec;beat_1sec];
beat_3sec=[beat_1sec;beat_1sec;beat_1sec];
beat_4sec=[beat_1sec;beat_1sec;beat_1sec;beat_1sec];
beat_5sec=[beat_1sec;beat_1sec;beat_1sec;beat_1sec;beat_1sec];

figure(1)
subplot(5,1,1)
    plot(beat_1sec)
subplot(5,1,2)
    plot(beat_2sec)
subplot(5,1,3)
    plot(beat_3sec)
subplot(5,1,4)
    plot(beat_4sec)
subplot(5,1,5)
    plot(beat_5sec)

figure(2)
subplot(5,1,1)
    imagesc(wt(boxpdf(beat_1sec),wavetype))
subplot(5,1,2)
    imagesc(wt(boxpdf(beat_2sec),wavetype))
subplot(5,1,3)
    imagesc(wt(boxpdf(beat_3sec),wavetype))
subplot(5,1,4)
    imagesc(wt(boxpdf(beat_4sec),wavetype))
subplot(5,1,5)
    imagesc(wt(boxpdf(beat_5sec),wavetype))

wavetype='PAUL';
figure(2)
subplot(5,1,1)
    imagesc(wt(boxpdf(beat_1sec),wavetype))
subplot(5,1,2)
    imagesc(wt(boxpdf(beat_2sec),wavetype))
subplot(5,1,3)
    imagesc(wt(boxpdf(beat_3sec),wavetype))
subplot(5,1,4)
    imagesc(wt(boxpdf(beat_4sec),wavetype))
subplot(5,1,5)
    imagesc(wt(boxpdf(beat_5sec),wavetype))

figure(5)    
    imagesc(wt(boxpdf(beat_5sec),wavetype))
    
sec_5=wt(boxpdf(beat_5sec),wavetype)  
beat_wt=sec_5;
for iii=1:41
    for iiii=1:45
        beat_wts2(iii,iiii)=max(beat_wt(iii,iiii*40-39:iiii*40));
    end
end
% wavetype='DOG';
% figure(4)
% subplot(5,1,1)
%     imagesc(wt(boxpdf(beat_1sec),wavetype))
% subplot(5,1,2)
%     imagesc(wt(boxpdf(beat_2sec),wavetype))
% subplot(5,1,3)
%     imagesc(wt(boxpdf(beat_3sec),wavetype))
% subplot(5,1,4)
%     imagesc(wt(boxpdf(beat_4sec),wavetype))
% subplot(5,1,5)
%     imagesc(wt(boxpdf(beat_5sec),wavetype))


