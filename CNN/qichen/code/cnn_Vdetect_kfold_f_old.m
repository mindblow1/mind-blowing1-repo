function [net, info] = cnn_Vdetect_kfold_f_old(varargin)
%CNN_SLPDB  Demonstrates MatConvNet on SLPDB

% run(fullfile(fileparts(mfilename('fullpath')),...
%   '..', '..', 'matlab', 'vl_setupnn.m')) ;
% diary on
% diary(['mitdb/cnn_output_',wavetype,'_',num2str(timeinsec),'_sec'])
run('./matconvnet-1.0-beta20/matlab/vl_setupnn.m') ;
opts.batchNormalization = false ;
opts.networkType = 'simplenn' ;
[opts, varargin] = vl_argparse(opts, varargin) ;

sfx = opts.networkType ;
if opts.batchNormalization, sfx = [sfx '-bnorm'] ; end
% opts.expDir = fullfile(vl_rootnn, 'data', ['mnist-baseline-' sfx]) ;
opts.expDir = './';
[opts, varargin] = vl_argparse(opts, varargin) ;

% opts.dataDir = fullfile(vl_rootnn, 'data', 'mnist') ;
opts.dataDir = './';
opts.imdbPath = ['imdb_5_unb.mat'];
% opts.imdbPath = 'capslpdb_imdb_4types_th20_CNN.mat';

opts.train = struct() ;
opts = vl_argparse(opts, varargin) ;
if ~isfield(opts.train, 'gpus'), opts.train.gpus = []; end;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

% net = cnn_slpdb_init_bayesselected('batchNormalization', opts.batchNormalization, ...
%                      'networkType', opts.networkType) ;
% net = cnn_slpdb_init('batchNormalization', opts.batchNormalization, ...
%                      'networkType', opts.networkType) ;


if exist(opts.imdbPath, 'file')
    load(opts.imdbPath) ;
    %   imdb=imdb2;
    %   imdb.images.data=single(imdb.images.data);
    %   imdb.images.data_mean=mean(imdb.images.data,4);
    %   imdb.images.data_mean=single(imdb.images.data_mean);
    %   for k=1:size(imdb.images.data,4)
    %       imdb.images.data(:,:,1,k)=imdb.images.data(:,:,1,k)-imdb.images.data_mean;
    %   end
    %   nn=size(imdb.images.data,4);
    %   imdb.images.set(1:round(nn*6/7))=1;
    %   imdb.images.set(round(nn*6/7)+1:nn)=3;
else
    imdb = getMnistImdb(opts) ;
    mkdir(opts.expDir) ;
    save(opts.imdbPath, '-struct', 'imdb') ;
end

net.meta.classes.name = arrayfun(@(x)sprintf('%d',x),1:10,'UniformOutput',false) ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

switch opts.networkType
    case 'simplenn', trainfn = @cnn_train ;
    case 'dagnn', trainfn = @cnn_train_dag ;
end

% 3 types; if 4 types, comment the next 3 lines
% imdb.images.labels(find(imdb.images.labels==2))=1;
% imdb.images.labels(find(imdb.images.labels==3))=2;
% imdb.images.labels(find(imdb.images.labels==4))=3;

% k=5;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for slpdb, combine slp01a and slp01b; slp02a and slp02b
% imdb.meta.casen(find(imdb.meta.casen==2))=1;
% imdb.meta.casen(find(imdb.meta.casen==4))=3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% u_db=unique(imdb.meta.casen);
% n_db=length(u_db);
% indices = crossvalind('Kfold',n_db,k);
% indices_udb = zeros(1,max(u_db));
% for i=1:length(u_db)
%     indices_udb(u_db(i))=i;
% end
% indices_case = zeros(1,length(imdb.meta.casen));
% for i=1:length(indices_case)
%     indices_case(i)=indices(indices_udb(imdb.meta.casen(i)));
% end
% % save('kfold_selection_slpdb_th20_4types_bs_00175_17','indices','indices_case');
% save(['kfold_selection_slpdb_th20_4types_ql_cc_34_',num2str(iter)],'indices','indices_case');
% % save(['kfold_selection_capslpdb_4types_th20_CNN_ql_cc_',num2str(iter)],'indices','indices_case');


imdb=imdb_unb;


opts.kfold=1;
% for k = 1:10

net = cnn_Vdetect_init_lqc_select_f('batchNormalization', opts.batchNormalization, ...
    'networkType', opts.networkType) ;

%     test = (imdb.images.set == 3); train = ~test;
%     imdb_train.images.data=imdb.images.data(:,:,:,train);
%     imdb_train.images.labels=imdb.images.labels(train);
%     imdb_train.images.data_mean=imdb.images.data_mean;
%     imdb_train.images.set=imdb.images.set(train);

%     [net, info] = trainfn(net, imdb_train, getBatch(opts), ...
%       'weightDecay',0.0117, ...
%       'expDir', opts.expDir, ...
%       net.meta.trainOpts, ...
%       opts.train, ...
%       'val', find(imdb_train.images.set == 3),'kfold',k) ;

%     [net, info] = trainfn(net, imdb_train, getBatch(opts), ...
%       'expDir', opts.expDir, ...
%       net.meta.trainOpts, ...
%       opts.train, ...
%       'gpus',1, ...
%       'val', find(imdb_train.images.set == 3)) ;

% [net, info] = trainfn(net, imdb_train, getBatch(opts), ...
%     'expDir', opts.expDir, ...
%     net.meta.trainOpts, ...
%     opts.train, ...
%     'val', find(imdb_train.images.set == 3)) ;

[net, info] = trainfn(net, imdb, getBatch(opts), ...
    'expDir', opts.expDir, ...
    net.meta.trainOpts, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;


%     imdb_test.images.data=imdb.images.data(:,:,:,test);
%     imdb_test.images.labels=imdb.images.labels(test);
%     imdb_test.images.data_mean=imdb.images.data_mean;
%     imdb_test.images.set=imdb.images.set(test);
%
%     [acc1(k), acc2(k), acc3(k), C_A1(:,:,k), C_A2(:,:,k), C_A3(:,:,k)] = cnn_test(net,imdb_train,100);
%
%     [acc21(k), acc22(k), acc23(k), C_A21(:,:,k), C_A22(:,:,k), C_A23(:,:,k)] = cnn_test(net,imdb_test,10);
%
%     net_total{k}=net;
net_total=net;

% end
% save('slpdb_kfold_output_th20_4types_bs_00175_17','-regexp','^acc','^C_A','indices');
save(['imdb_output_unb_net'],'net_total');


% save(['mitdb/Vdetect_imdb_output_',wavetype,'_',num2str(timeinsec*10),'_sec_net_',num2str(iter)],'net_total');
% save(['mitdb/Vdetect_imdb_output_',wavetype,'_',num2str(timeinsec),'_sec'],'-regexp','^acc','^C_A');
% save(['capslpdb_kfold_output_th20_4types_ql_cc_',num2str(iter)],'-regexp','^acc','^C_A','indices');
% diary off

% --------------------------------------------------------------------
function fn = getBatch(opts)
% --------------------------------------------------------------------
switch lower(opts.networkType)
    case 'simplenn'
        fn = @(x,y) getSimpleNNBatch(x,y) ;
    case 'dagnn'
        bopts = struct('numGpus', numel(opts.train.gpus)) ;
        fn = @(x,y) getDagNNBatch(bopts,x,y) ;
end

% --------------------------------------------------------------------
function [images, labels] = getSimpleNNBatch(imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;

% --------------------------------------------------------------------
function inputs = getDagNNBatch(opts, imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;
if opts.numGpus > 0
    images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

% --------------------------------------------------------------------
function imdb = getMnistImdb(opts)
% --------------------------------------------------------------------
% Preapre the imdb structure, returns image data with mean image subtracted
files = {'train-images-idx3-ubyte', ...
    'train-labels-idx1-ubyte', ...
    't10k-images-idx3-ubyte', ...
    't10k-labels-idx1-ubyte'} ;

if ~exist(opts.dataDir, 'dir')
    mkdir(opts.dataDir) ;
end

for i=1:4
    if ~exist(fullfile(opts.dataDir, files{i}), 'file')
        url = sprintf('http://yann.lecun.com/exdb/mnist/%s.gz',files{i}) ;
        fprintf('downloading %s\n', url) ;
        gunzip(url, opts.dataDir) ;
    end
end

f=fopen(fullfile(opts.dataDir, 'train-images-idx3-ubyte'),'r') ;
x1=fread(f,inf,'uint8');
fclose(f) ;
x1=permute(reshape(x1(17:end),28,28,60e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 't10k-images-idx3-ubyte'),'r') ;
x2=fread(f,inf,'uint8');
fclose(f) ;
x2=permute(reshape(x2(17:end),28,28,10e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 'train-labels-idx1-ubyte'),'r') ;
y1=fread(f,inf,'uint8');
fclose(f) ;
y1=double(y1(9:end)')+1 ;

f=fopen(fullfile(opts.dataDir, 't10k-labels-idx1-ubyte'),'r') ;
y2=fread(f,inf,'uint8');
fclose(f) ;
y2=double(y2(9:end)')+1 ;

set = [ones(1,numel(y1)) 3*ones(1,numel(y2))];
data = single(reshape(cat(3, x1, x2),28,28,1,[]));
dataMean = mean(data(:,:,:,set == 1), 4);
data = bsxfun(@minus, data, dataMean) ;

imdb.images.data = data ;
imdb.images.data_mean = dataMean;
imdb.images.labels = cat(2, y1, y2) ;
imdb.images.set = set ;
imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:9,'uniformoutput',false) ;




% 0 cnn (5,8,1,50) (3,4,50,100) (7,8,100,500) learningRate 0.001 numEpochs 20 batchSize 100
% 2-20 learningRate 0.001*iter
%
%
%
%