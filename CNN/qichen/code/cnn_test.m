function [acc1, acc2, acc3, C_A1, C_A2, C_A3] = cnn_test(net,imdb,split,imdb_train)
% function [acc3, C_A3] = cnn_test(net,imdb,split,imdb_train)
% test the CNN model 
%
if nargin==4 % adjust mean of imdb by imdb_train
    for k=1:size(imdb.images.data,4)
        imdb.images.data(:,:,1,k)=imdb.images.data(:,:,1,k)+imdb.images.data_mean-imdb_train.images.data_mean;
    end
end

if nargin<3
    split=1;
end

%run('/opt/matconvnet-1.0-beta17/matlab/vl_setupnn.m');
net.layers{1,end}.type='softmax';
if split==1
    res=vl_simplenn(net,imdb.images.data);
    for i=1:max(imdb.images.labels)
        for j=1:size(res(end).x,4)
            ret(i,j)=res(end).x(1,1,i,j);
        end
    end
    [m im]=max(ret);
    type=imdb.images.labels;
    tp=im-type;
    acc1=length(find(tp==0))/length(type)
    C_A1=confusionmat(im,type,'order',[1:max(imdb.images.labels)])

    % training
    disp('training');
    training=find(imdb.images.set==1);
    test=find(imdb.images.set==3);
    res=vl_simplenn(net,imdb.images.data(:,:,:,training));
    ret=[];
    for i=1:max(imdb.images.labels)
        for j=1:size(res(end).x,4)
            ret(i,j)=res(end).x(1,1,i,j);
        end
    end
    [m im]=max(ret);
    type=imdb.images.labels(training);
    tp=im-type;
    acc2=length(find(tp==0))/length(type)
    C_A2=confusionmat(im,type,'order',[1:max(imdb.images.labels)])

    % test
    disp('test');
    res=vl_simplenn(net,imdb.images.data(:,:,:,test));
    ret=[];
    for i=1:max(imdb.images.labels)
        for j=1:size(res(end).x,4)
            ret(i,j)=res(end).x(1,1,i,j);
        end
    end
    [m im]=max(ret);
    type=imdb.images.labels(test);
    tp=im-type;
    acc3=length(find(tp==0))/length(type)
    C_A3=confusionmat(im,type,'order',[1:max(imdb.images.labels)])
else
    type=[];
    tp=[];
    result=[];
    for k=1:split
        each=ceil(size(imdb.images.data,4)/split);
        res=vl_simplenn(net,imdb.images.data(:,:,:,(k-1)*each+1:min(k*each,size(imdb.images.data,4))));
        ret=[];
        for i=1:max(imdb.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type0=imdb.images.labels((k-1)*each+1:min(k*each,size(imdb.images.data,4)));
        type=[type type0];
        if ~isempty(im)
            tp=[tp im-type0];
            result=[result im];
        end
    end
    acc1=length(find(tp==0))/length(type)
    C_A1=confusionmat(result,type,'order',[1:max(imdb.images.labels)])

%     training
    disp('training');
    training=find(imdb.images.set==1);
    data_train=imdb.images.data(:,:,:,training);
    label_train=imdb.images.labels(training);
    type=[];
    tp=[];
    result=[];
    for k=1:split
        each=ceil(size(data_train,4)/split);
        res=vl_simplenn(net,data_train(:,:,:,(k-1)*each+1:min(k*each,size(data_train,4))));
        ret=[];
        for i=1:max(imdb.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type0=label_train((k-1)*each+1:min(k*each,size(data_train,4)));
        type=[type type0];
        if ~isempty(im)
            tp=[tp im-type0];
            result=[result im];
        end
    end
    acc2=length(find(tp==0))/length(type)
    C_A2=confusionmat(result,type,'order',[1:max(imdb.images.labels)])

    % test
    disp('test');
    test=find(imdb.images.set==3);
    data_test=imdb.images.data(:,:,:,test);
    label_test=imdb.images.labels(test);
    type=[];
    tp=[];
    result=[];
    for k=1:split
        each=ceil(size(data_test,4)/split);
        res=vl_simplenn(net,data_test(:,:,:,(k-1)*each+1:min(k*each,size(data_test,4))));
        ret=[];
%         res=vl_simplenn(net,imdb.images.data(:,:,:,test));
%         ret=[];
        for i=1:max(imdb.images.labels)
            for j=1:size(res(end).x,4)
                ret(i,j)=res(end).x(1,1,i,j);
            end
        end
        [m im]=max(ret);
        type0=label_test((k-1)*each+1:min(k*each,size(data_test,4)));
        type=[type type0];
%         type=imdb.images.labels(test);
%         tp=im-type;
        if ~isempty(im)
            tp=[tp im-type0];
            result=[result im];
        end
    end
    acc3=length(find(tp==0))/length(type)
    C_A3=confusionmat(result,type,'order',[1:max(imdb.images.labels)])
end
end