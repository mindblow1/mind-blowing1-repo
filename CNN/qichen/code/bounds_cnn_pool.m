function [kmin,kmax,ip_min]=bounds_cnn_pool(numLayers,inDimension,min_size)
% This function computes the upper bound of the kernel size in each layer
% of the CNN. It computes the bound only along a single dimension

ip_dim=inDimension;
n=numLayers;
if n==1
    kmin=(min_size)*ones(1,2);
    kmax=[inDimension,0];
    ip_min=inDimension;
else
    kmin=(min_size)*ones(1,n); %The minimum kernel size for each layer
    % kmin=[3,2,3,3]; %The minimum kernel size for each layer
    kmax=zeros(1,n);
    op_min=zeros(1,n);
    ip_min=zeros(1,n);
    ip_max=zeros(1,n);
    ip_min(n)=kmin(n);

    %Compute the minimum size of input for each layer
    for i=n-1:-1:2
        op_min(i)=ip_min(i+1);
        ip_min(i)=op_min(i)+kmin(n-1)-1;
    end

    %Compute the upper bound of Kernel size for each layer
    for i=1:n-1
        if i==1
            kmax(i)=floor(ip_dim/2)-ip_min(i+1)+1;
            ip_max(i+1)=floor(ip_dim/2)-kmin(i)+1;
        else
            kmax(i)=floor(ip_max(i)/2)-ip_min(i+1)+1;
            ip_max(i+1)=floor(ip_max(i)/2)-kmin(i)+1;
        end   
    end
end
end