function storeimdb()
% Fucntion to store imdb data.
% Currently no inputs required.
% imdb.mat and imdb_unb.mat are generated (hopefully)
% with imdb_unb.mat being the full image database
% and imdb.mat being the balanced dataset.
% Further adjustments may be made in the future.


% % introducing a Hanning window
% Hw=hanning(360*timeinsec);

tic

% The tif stacks are read in and recorded separately using the sos package
stack  = sos_read_data('/image.tif');
stack2 = sos_read_data('/synapse.tif');

[stack_x,stack_y,stack_z] = size(stack);
% [stack2_x,stack2_y,stack2_z] = size(stack2);
% For the time being we assume stack and stack2 have same sizes...

% Initialise the number of windows stored
nth_win=0;

% Initialise edge deselection number
n_pixel=5;

% Initialise synapse and non-synapse counts
S=0;
nonS=0;

% number of layers
n_layers=1;

% Initialise unbalanced database
imdb_unb.images.data=zeros(2*n_pixel+1,2*n_pixel+1,1,(stack_x-2*n_pixel)*(stack_x-2*n_pixel)*n_layers, 'single');
imdb_unb.images.labels=zeros(1,(stack_x-2*n_pixel)*(stack_x-2*n_pixel)*n_layers, 'single');
imdb_unb.images.set=zeros(1,(stack_x-2*n_pixel)*(stack_x-2*n_pixel)*n_layers, 'single');

% count the number of points to analyse
for z=1:1   % eventually we would set the latter value to stack_z for the whole stack
    for x=1:stack_x
        for y=1:stack_y
            % determine if the pixel of selection is not on the edge
            if x<=n_pixel || stack_x-x<n_pixel
            elseif y<=n_pixel || stack_y-y<n_pixel
            else
                switch stack2(x,y,z)
                    case 1
                        S=S+1;
                        im=stack(x-n_pixel:x+n_pixel,y-n_pixel:y+n_pixel,z);
                        nth_win=nth_win+1;
                        
                        %                         figure(2)
                        %                         colormap gray
                        %                         imagesc(im)
                        %                         title('synapse')
                        %                         xlabel('x')
                        %                         ylabel('y')
                        %                         grid on
                        %                         % plot the window
                        %                         drawnow
                        
                        % beat_wt=resample(beat_wt',45,360*timeinsec)';
                        imdb_unb.images.data(:,:,1,nth_win)=im;
                        imdb_unb.images.labels(nth_win)=2;  % 2 for S
                        
                        
                    case 0
                        nonS=nonS+1;
                        im=stack(x-n_pixel:x+n_pixel,y-n_pixel:y+n_pixel,z);
                        nth_win=nth_win+1;
                        
                        %                         figure(1)
                        %                         colormap gray
                        %                         imagesc(im)
                        %                         title('non-synapse')
                        %                         xlabel('x')
                        %                         ylabel('y')
                        %                         grid on
                        %                         % plot the window
                        %                         drawnow
                        
                        % beat_wt=resample(beat_wt',45,360*timeinsec)';
                        imdb_unb.images.data(:,:,1,nth_win)=im;
                        imdb_unb.images.labels(nth_win)=1;  % 1 for nonS
                        
                end
            end
        end
    end
end


nth_win

% S for synapse; nonS for not synapse...
% with size stack_z
S_all(z)=S;
nonS_all(z)=nonS;

% clear ann chan comments num signal subtype tm type


% save imdb (balanced)

% singularise the image values
imdb_unb.images.data=single(imdb_unb.images.data);

% sort data for CNN

% for unbalanced data
% set imdb_unb.images.data_mean
% avoid NaN, Inf
imdb_unb.images.data(find(isnan(imdb_unb.images.data)))=0;
imdb_unb.images.data(find(isinf(imdb_unb.images.data)))=0;

imdb_unb.images.data_mean=mean(imdb_unb.images.data,4);
imdb_unb.images.data_mean=single(imdb_unb.images.data_mean);
imdb.images.data_mean=imdb_unb.images.data_mean;
for k=1:size(imdb_unb.images.data,4)
    imdb_unb.images.data(:,:,1,k)=imdb_unb.images.data(:,:,1,k)-imdb_unb.images.data_mean;
end
% set imdb_unb.images.set, 5/6 as training, 1/6 as validation
nn=size(imdb_unb.images.data,4);
indices = crossvalind('Kfold',nn,6); %%%%
imdb_unb.images.set(indices<6)=1;
imdb_unb.images.set(indices==6)=3;

% % %  split the data into 10 sections to eliminate self validation
% % %  train 9 sections and validate 1
% mm=length(unique(imdb_unb.meta.casen));
% % indices_casen = crossvalind('Kfold',mm,10); %%%%
% for ii=1:mm
%     imdb_unb.meta.casekfold(imdb_unb.meta.casen==ii)=indices_casen(ii);
% end

% %  produce a balanced imdb

% Initialise balanced database
imdb.images.data=zeros(2*n_pixel+1,2*n_pixel+1,1,2*sum(S_all));
imdb.images.labels=zeros(1,2*sum(S_all));
imdb.images.set=zeros(1,2*sum(S_all));

% initialise nth window
nth_win=1;

% find the number of S pixels
S_label=find(imdb_unb.images.labels==2);
S_total=length(S_label);
% find the number of nonS pixels
nonS_label=find(imdb_unb.images.labels==1);
nonS_total=length(nonS_label);
% randomly select the same number of nonS pixels to match the S pixels
temparray1=randperm(nonS_total);
temparray1=temparray1(1:S_total);
% nonS is the nth pixels from the origninal data that is nonS
nonS=nonS_label(temparray1);
% S is the nth pixel from the origninal data that is S
S=S_label;

% now we store imdb
for iii=1:length(nonS)
    imdb.images.data(:,:,1,nth_win)=imdb_unb.images.data(:,:,1,nonS(iii));
    imdb.images.labels(nth_win)=imdb_unb.images.labels(nonS(iii));
    imdb.images.set(nth_win)=imdb_unb.images.set(nonS(iii));
    nth_win=nth_win+1;
end
for iii=1:length(S)
    imdb.images.data(:,:,1,nth_win)=imdb_unb.images.data(:,:,1,S(iii));
    imdb.images.labels(nth_win)=imdb_unb.images.labels(S(iii));
    imdb.images.set(nth_win)=imdb_unb.images.set(S(iii));
    nth_win=nth_win+1;
end


save(['imdb_',num2str(n_pixel),'_unb'],'imdb','imdb_unb','-v7.3');
save(['imdb_',num2str(n_pixel)],'imdb','-v7.3');

toc
end