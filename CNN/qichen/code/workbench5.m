% % workbench5

% for i=1:10
%     aa=find(indices_casen==i);
%     i
%     bb=fileno(aa)
%     for ii=1:length(aa)
%         V(ii)=sum(imdb_unb.meta.casen==aa(ii) & imdb_unb.images.labels==2);
%         nonV(ii)=sum(imdb_unb.meta.casen==aa(ii) & imdb_unb.images.labels==1);
%     end
%     V
%     nonV
% end

% fileno=[100,101,102,103,104,105,106,107,108,109,111,112,...
%     113,114,115,116,117,118,119,121,122,123,124,200,...
%     201,202,203,205,207,208,209,210,212,213,214,215,...
%     217,219,220,221,222,223,228,230,231,232,233,234];
%
% for ii=1:3
%     switch ii
%         case 1
%             wavetype='PAUL';
%         case 2
%             wavetype='MORLET';
%         case 3
%             wavetype='DOG';
%     end
%
%     for i=1:12
%         load(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat.mat'])
%         A1(i)=mean(stat.acc1);
%         B1(i)=mean(stat.F11);
%         C1(i)=mean(stat.Se1);
%         D1(i)=mean(stat.Sp1);
%         Ae1(i)=std(stat.acc1);
%         Be1(i)=std(stat.F11);
%         Ce1(i)=std(stat.Se1);
%         De1(i)=std(stat.Sp1);
%
%         A2(i)=mean(stat.acc2);
%         B2(i)=mean(stat.F12);
%         C2(i)=mean(stat.Se2);
%         D2(i)=mean(stat.Sp2);
%         Ae2(i)=std(stat.acc2);
%         Be2(i)=std(stat.F12);
%         Ce2(i)=std(stat.Se2);
%         De2(i)=std(stat.Sp2);
%
% %         for iii=1:48
% %             fn(iii)=sum(stat.er_fn==iii);
% %             fp(iii)=sum(stat.er_fp==iii);
% %         end
% %         figure
% %         pareto(fn)
% %         tt_fn=sprintf('%s %d fn',wavetype,i*5);
% %         title(tt_fn)
% %
% %         figure
% %         pareto(fp)
% %         tt_fp=sprintf('%s %d fp',wavetype,i*5);
% %         title(tt_fp)
% %
% %         fprintf('wavetype: %s %d\n',wavetype,i*5);
% %         fprintf('fn\n');
% %         tabulate(stat.er_fn)
% %         fprintf('fp\n');
% %         tabulate(stat.er_fp)
%
%         %                 A3(i)=mean(stat.acc3);
%         %                 B3(i)=mean(stat.F13);
%         %                 C3(i)=mean(stat.Se3);
%         %                 D3(i)=mean(stat.Sp3);
%         %                 Ae3(i)=std(stat.acc3);
%         %                 Be3(i)=std(stat.F13);
%         %                 Ce3(i)=std(stat.Se3);
%         %                 De3(i)=std(stat.Sp3);
%     end
%
%     figure
%     hold on
%     plot(A1,'b')
%     plot(B1,'r')
%     plot(C1,'g')
%     plot(D1,'k')
%     errorbar(1:12,A1,Ae1,'b')
%     errorbar(1:12,B1,Be1,'r')
%     errorbar(1:12,C1,Ce1,'g')
%     errorbar(1:12,D1,De1,'k')
%     title([wavetype,' test']);
% end


% for ii=1:3
%     
%     switch ii
%         case 1
%             wavetype='PAUL';
%         case 2
%             wavetype='MORLET';
%         case 3
%             wavetype='DOG';
%     end
%     
%     for i=1:12
%         load(['Vdetect_imdb_output_',wavetype,'_',num2str(i*5),'_sec_stat.mat'])
%         A1(i)=mean(stat.acc1);
%         B1(i)=mean(stat.F11);
%         Ae1(i)=std(stat.acc1);
%         Be1(i)=std(stat.F11);
%         
%         A2(i)=mean(stat.acc2);
%         B2(i)=mean(stat.F12);
%         Ae2(i)=std(stat.acc2);
%         Be2(i)=std(stat.F12);
%         
%     end
%     
%     figure
%     hold on
%     
%     %     plot(A1,'b')
%     %     plot(A2,'r')
%     %     errorbar(1:12,A1,Ae1,'b')
%     %     errorbar(1:12,A2,Ae2,'r')
%     
%     plot(B1,'b')
%     plot(B2,'r')
%     errorbar(1:12,B1,Be1,'b')
%     errorbar(1:12,B2,Be2,'r')
%     
%     title(wavetype);
%     ax = gca;
%     ax.XTick=[1 2 3 4 5 6 7 8 9 10 11 12];
%     ax.XTickLabel={'0.5','1','1.5','2','2.5','3','3.5','4','4.5','5','5.5','6'};
%     xlabel('Window size in seconds')
%     axis([0 13 0.55 1.05])
%     legend('test','train','location','southeast')
%     grid on
%     
% end

for k=1:10
p1(k)=stat.tp1(k)/(stat.tp1(k)+stat.fp1(k));
Se1(k)=stat.tp1(k)/(stat.tp1(k)+stat.fn1(k));
Sp1(k)=stat.tn1(k)/(stat.tn1(k)+stat.fp1(k));
acc1(k)=(stat.tp1(k)+stat.tn1(k))/sum(sum(stat.C_A1(:,:,k)));
Macc1(k)=2*stat.Se1(k)*stat.Sp1(k)/(stat.Se1(k)+stat.Sp1(k));
F1(k)=2*stat.Se1(k)*p1(k)/(stat.Se1(k)+p1(k));
p2(k)=stat.tp2(k)/(stat.tp2(k)+stat.fp2(k));
Se2(k)=stat.tp2(k)/(stat.tp2(k)+stat.fn2(k));
Sp2(k)=stat.tn2(k)/(stat.tn2(k)+stat.fp2(k));
acc2(k)=(stat.tp2(k)+stat.tn2(k))/sum(sum(stat.C_A2(:,:,k)));
Macc2(k)=2*stat.Se2(k)*stat.Sp2(k)/(stat.Se2(k)+stat.Sp2(k));
F2(k)=2*stat.Se2(k)*p2(k)/(stat.Se2(k)+p2(k));
end

