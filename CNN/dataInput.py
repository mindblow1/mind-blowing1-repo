# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 15:40:02 2017

@author: user
"""
from PIL import Image
import numpy as np

def readTif(address, readDepth = 0, steps = (1,1,1), fromBottom = False):
    """ Reads the TIF at address and returns it as a numpy array of values from 0 to 255 """
    #if readDepth is 0, use the whole stack
    #steps goes (x_step, y_step, z_step)
    x_step, y_step, z_step = steps
    
    image = Image.open(address)
    fileDepth = 0
    while True:
        fileDepth += 1
        try:
            image.seek(fileDepth)
        except EOFError:
            break
    
    assert(readDepth <= fileDepth)
    if readDepth == 0:
        readDepth = fileDepth
    
    width, height = image.size
    
    if fromBottom:
        direction = -1
        startLayer = readDepth-1
        finishLayer = -1
    else:
        direction = 1
        startLayer = 0
        finishLayer = readDepth
    #create array of right dimensions
    Array = np.zeros([int(np.ceil(readDepth/steps[2])), int(np.ceil(height/steps[1])), int(np.ceil(width/steps[0]))])
    for k in range(startLayer, finishLayer, steps[2]*direction):  #iterate through each layer - either going up or down
        image.seek(k)
        layer = np.array(image)
        if x_step == 1 and y_step == 1:
            Array[k//z_step] = layer
        else:
            for j in range(0, height, steps[1]):
                for i in range(0, width, steps[0]):
                    Array[k//z_step][j//y_step][i//x_step] = layer[j][i]
    return Array

def createNeighbourSets(EMimage, synapses, n, steps = (1,1,1), nz = 0):
    """ Creates a numpy array of the neighbours in the (2*n+1)*(2*nz+1) grid around each pixel,
    moving in steps given by 'steps' """
    #EMimage and synapses should both be numpy arrays of same dimensions
    assert (EMimage.shape == synapses.shape)
    x_step, y_step, z_step = steps
    depth, height, width = EMimage.shape
    
    numDataPoints = int(np.ceil((height-2*n)/y_step)*np.ceil((width-2*n)/x_step)*np.ceil((depth-2*nz)/z_step))
    dimensions = (2*n+1)**2*(2*nz+1)
    
    #count the synapses
    numSynapses = 0
    for k in range(nz, depth-nz, z_step):
        for j in range(n, height-n, y_step):
            for i in range(n, width-n, x_step):
                if synapses[k][j][i]:
                    numSynapses += 1
    
    Synapses = np.zeros([numSynapses, dimensions])
    notSynapses = np.zeros([numDataPoints-numSynapses, dimensions])
    
    synapsesAdded = 0
    notSynapsesAdded = 0
    for k in range(nz, depth-nz, z_step):
        for j in range(n, height-n, y_step):
            for i in range(n, width-n, x_step):
                #compile a list of all the neighbours of the pixel
                neighbours = np.zeros([dimensions])
                neighboursAdded = 0
                for dk in range(-nz, nz+1):
                    for dj in range(-n, n+1):
                        for di in range(-n, n+1):
                            neighbours[neighboursAdded] = EMimage[k+dk][j+dj][i+di]
                            neighboursAdded += 1
                            
                if synapses[k][j][i]:
                    Synapses[synapsesAdded] = neighbours
                    synapsesAdded += 1
                else:
                    notSynapses[notSynapsesAdded] = neighbours
                    notSynapsesAdded += 1
    assert(synapsesAdded == numSynapses)
    assert(notSynapsesAdded == numDataPoints-numSynapses)
    
    return (Synapses, notSynapses)

""" some tests """
if __name__ == '__main__':
    em = readTif("C:/Users/user/Python/envs/tensorflow/train/image.tif")
    sy = readTif("C:/Users/user/Python/envs/tensorflow/train/synapse.tif")
    a = createNeighbourSets(em, sy, 3, steps = (50,50,20))
    del em, sy