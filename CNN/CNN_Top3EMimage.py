""" trains a 2d CNN on the top 3 images from the stack"""

from PIL import Image
import numpy as np
import tensorflow as tf
import random
sess = tf.InteractiveSession()

#from tensorflow.examples.tutorials.mnist import input_data
#mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
print('got data')

EMimageTIF = Image.open("C:/Users/user/Python/envs/tensorflow/train/image.tif")
synapseTIF = Image.open("C:/Users/user/Python/envs/tensorflow/train/synapse.tif")
EMimage1 = np.array(EMimageTIF)
synapse1 = np.array(synapseTIF)//255
EMimageTIF.seek(2)
synapseTIF.seek(2)
EMimage2 = np.array(EMimageTIF)
synapse2 = np.array(synapseTIF)//255
EMimageTIF.seek(3)
synapseTIF.seek(3)
EMimage3 = np.array(EMimageTIF)
synapse3 = np.array(synapseTIF)//255
EMimages = [EMimage1, EMimage2, EMimage3]
synapses = [synapse1, synapse2, synapse3]

n = 5           #size of conv filter is 2n + 1

#numSynapses = sum(sum(synapse))
numSynapses = 0
for i in range(n, 1024-n):
    for j in range(n, 1024-n):
        numSynapses += synapse1[j][i] + synapse2[j][i] + synapse3[j][i]

#take 11x11 neighbourhood so use 1014x1014
not_synapseX = np.ndarray([3*(1024-2*n)**2-numSynapses, (2*n+1)**2])
not_synapseY = [[1,0]]*(3*(1024-2*n)**2-numSynapses)
synapseX = np.ndarray([numSynapses, (2*n+1)**2])
synapseY = [[0,1]]*(numSynapses)

synapses_added = 0
not_synapses_added = 0
for image in range(3):
    for i in range(5, 1024-n):
        for j in range(5, 1024-n):
            # add pixel to the data
            neighbourhood = np.zeros([(2*n+1)**2])
            index = 0
            for di in range(-n, n+1):
                for dj in range(-n, n+1):
                    neighbourhood[index] = EMimages[image][j+dj][i+di]
                    index += 1
            if synapses[image][j][i]:
                synapseX[synapses_added] = neighbourhood
                synapses_added += 1
            else:
                not_synapseX[not_synapses_added] = neighbourhood
                not_synapses_added += 1

assert(synapses_added == numSynapses)
assert(not_synapses_added == 3*(1024-2*n)**2 - numSynapses)

def dataSample(not_synapseX, synapseX, numPoints):
    synSamples = numPoints//2
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*n+1)**2])
    for point in range(synSamples):
        data[point] = synapseX[synapseIndex[point]]
    for point in range(notSynSamples):
        data[synSamples+point] = not_synapseX[notSynapseIndex[point]]
    y = np.array([[0,1]]*synSamples+[[1,0]]*notSynSamples)

    return (data, y)


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')


x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y_ = tf.placeholder(tf.float32, shape = [None, 2])

#W_conv1 = weight_variable([3, 3, 1, 32])
W_conv1 = weight_variable([4, 4, 1, 32])
b_conv1 = bias_variable([32])

x_image = tf.reshape(x, [-1,11,11,1])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)    #is now 8x8(x32)
#h_pool1 = max_pool_2x2(h_conv1)

#W_conv2 = weight_variable([5, 5, 32, 64])
#b_conv2 = bias_variable([64])

#h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
#h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([8 * 8 * 32, 1024])
b_fc1 = bias_variable([1024])

h_conv1_flat = tf.reshape(h_conv1, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 2])
b_fc2 = bias_variable([2])

y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

print('training starts now matey')

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.global_variables_initializer())

for i in range(20000):
  batch = dataSample(not_synapseX, synapseX, 100)
  if i%100 == 0:
    train_accuracy = accuracy.eval(feed_dict={
        x:batch[0], y_: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

######## make test sets
EMimageTIF.seek(124)
synapseTIF.seek(124)
EMimageTEST = np.array(EMimageTIF)
synapseTEST = np.array(synapseTIF)//255

XData = np.ndarray([1014**2, 121])
YData = np.ndarray([1014**2, 2])

pixelsAdded=0
for i in range(n, 1024-n):
    for j in range(n, 1024-n):
        # add pixel to the data
        neighbourhood = np.zeros([121])
        index = 0
        for di in range(-n, n+1):
            for dj in range(-n, n+1):
                neighbourhood[index] = EMimageTEST[j+dj][i+di]
                index += 1
        XData[pixelsAdded] = neighbourhood
        if synapseTEST[j][i]:
            YData[pixelsAdded] = [0, 1]
        else:
            YData[pixelsAdded] = [1, 0]
        pixelsAdded += 1
#######
print('got to here')

#print("test accuracy %g"%accuracy.eval(feed_dict={
#    x: XData, y_: YData, keep_prob: 1.0}))

PredMap = np.zeros([1014**2])
for MIN in range(0, 1014**2, 39):
    MAX = MIN + 39
    pred = sess.run([y_conv], feed_dict={ x: XData[MIN:MAX], y_: YData[MIN:MAX], keep_prob: 1.0})[0]
    for index in range(MIN, MAX):
        if pred[index%39][1] > pred[index%39][0]:
            PredMap[index] = 1

RealMap = np.zeros([1014**2])
for i in range(0,len(YData)):
	if YData[i][1]:
		RealMap[i] = 1
##
##
##for mult in [0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4]:
##    TP=0
##    FP=0
##    FN=0
##    TN=0
##    for datum in range(MAX):
##        pred_syn = (pred[datum][1] > pred[datum][0]*mult)
##        true_syn = YData[datum][1]
##        if true_syn and pred_syn:
##            TP += 1
##        elif (not true_syn) and pred_syn:
##            FP += 1
##        elif true_syn and (not pred_syn):
##            FN += 1
##        elif (not true_syn) and (not pred_syn):
##            TN += 1
##    prec = TP/(TP+FP)
##    rec = TP/(TP+FN)
##    F1 = 2*(prec*rec)/(prec+rec)
##    print("TP:", TP, "FP:", FP, "FN:", FN, "TN:", TN, " so F1 is", F1, "for mult of", mult)
##    
##
