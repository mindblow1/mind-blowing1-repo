# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 23:03:39 2017

@author: user
"""
name = 'gpu_25x25x3_rotation2'
import dataInputNoPIL as dataInput
import numpy as np
import random
import tensorflow as tf
from CNN import *
sess = tf.InteractiveSession()

###### SET PARAMETERS
#trainingStepSize = 1e-4
#numIterations = 10000
#sizeBatches = 100
#proportionSynapses = 0.05
n = 12 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 1#vertical length is 2n+1
thresholdProb1 = 0.4
thresholdProb2 = 0.5
thresholdProb3 = 0.6
thresholdProb4 = 0.7

global threeD 
threeD = nz>0

def getPredictions(sess, xData, n, nz):
    if nz>0:
        xData = np.reshape(xData, [-1, (2*nz+1), (2*n+1)**2])
    #note it doesn't matter what y1 is set to
    ans = np.ndarray([xData.shape[0],1])
    for MIN in range(0,xData.shape[0],100):
        MAX = min(MIN+100, xData.shape[0])
        result = sess.run([probSynapse], feed_dict={ x: xData[MIN:MAX], y1: [0]*100, keep_prob: 1.0})[0]
        ans[MIN:MAX] = result
    return ans


####    LOAD ALL THE WEIGHTS AND BIASES

weights = np.load('/home/threeyp/mindblowing/saved_nets/weights_for_'+name+'overfitted.npz')
names = weights.items()[1][1]
values = weights.items()[0][1]
try:
    for i in range(len(names)):
        var_name = names[i]
        array = values[i]
        #exec(name + ' = values[i]')
        print(type(var_name), type(array))
        exec(var_name + ' = tf.Variable(values[i])')
except TypeError:
    names, values = values, names
    for i in range(len(names)):
        var_name = names[i]
        array = values[i]
        #exec(name + ' = values[i]')
        print(type(var_name), type(array))
        exec(var_name + ' = tf.Variable(values[i])')


##### DEFINE CNN
if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)


if threeD:
    x_image = tf.reshape(x, [-1,2*nz+1,2*n+1,2*n+1, 1])
#else:
#    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])

h_conv0 = tf.nn.relu(conv(x_image, W_conv0_o, threeD) + b_conv0_o)  #22x22
h_conv0 = tf.reshape(h_conv0, [-1,22,22,16])
threeD = 0


h_conv1 = tf.nn.relu(tf.nn.conv2d(h_conv0, W_conv1_o, strides = [1, 1, 1, 1], padding = 'SAME') + b_conv1_o)

h_pooled0 = tf.nn.relu(tf.nn.conv2d(h_conv1, W_conv2_o, strides = [1, 2, 2, 1], padding = 'VALID') + b_conv2_o) #11x11        #11x11

h_conv3 = tf.nn.relu(conv(h_pooled0, W_conv3_o, threeD) + b_conv3_o) #8x8

h_conv_flat = tf.reshape(h_conv3, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv_flat, W_fc1_o) + b_fc1_o)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o)
h_fc3 = tf.nn.relu(tf.matmul(h_fc2, W_fc3_o) + b_fc3_o)

y_conv = tf.nn.relu(tf.matmul(h_fc3, W_fc4_o) + b_fc4_o)

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
#train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.initialize_all_variables())


###### LOAD TEST DATA
imageTEST = np.load("npy_train/imageTEST25.npy")
synapsesTEST = np.load("npy_train/synapseTEST25.npy")
full_tp1, full_fn1, full_fp1 = 0, 0, 0
full_tp2, full_fn2, full_fp2 = 0, 0, 0
full_tp3, full_fn3, full_fp3 = 0, 0, 0
full_tp4, full_fn4, full_fp4 = 0, 0, 0
for slic in range(1024-2*n):
    from time import time
    start = time()
    test_synapseX, test_not_synapseX = dataInput.createNeighbourSets(imageTEST, synapsesTEST, n, nz = nz, steps = (1,5000,1),offset = (0,slic,0))
    print(slic, test_synapseX.shape, test_not_synapseX.shape, 'neigbour sets in ', time()-start)
    test_synapse_predictions = getPredictions(sess, test_synapseX, n, nz)
    test_not_synapse_predictions = getPredictions(sess, test_not_synapseX, n, nz)

######## CALCULATE F1 ON TEST DATA
    _, TP1, FN1, FP1, TN1 = calcF1(test_synapse_predictions, test_not_synapse_predictions, thresholdProb1, andMore=True)
    _, TP2, FN2, FP2, TN2 = calcF1(test_synapse_predictions, test_not_synapse_predictions, thresholdProb2, andMore=True)
    _, TP3, FN3, FP3, TN3 = calcF1(test_synapse_predictions, test_not_synapse_predictions, thresholdProb3, andMore=True)
    _, TP4, FN4, FP4, TN4 = calcF1(test_synapse_predictions, test_not_synapse_predictions, thresholdProb4, andMore=True)
    print('threshold 1, TP:', TP1, 'FN:', FN1, 'FP:', FP1, 'TN:', TN1, '\n')
    full_tp1, full_fn1, full_fp1 = full_tp1+TP1, full_fn1+FN1, full_fp1+FP1
    full_tp2, full_fn2, full_fp2 = full_tp2+TP2, full_fn2+FN2, full_fp2+FP2
    full_tp3, full_fn3, full_fp3 = full_tp3+TP3, full_fn3+FN3, full_fp3+FP3
    full_tp4, full_fn4, full_fp4 = full_tp4+TP4, full_fn4+FN4, full_fp4+FP4
    p1 = full_tp1/(full_tp1+full_fp1)
    r1 = full_tp1/(full_tp1+full_fn1)
    F1 = 2*p1*r1/(p1+r1)
    p2 = full_tp2/(full_tp2+full_fp2)
    r2 = full_tp2/(full_tp2+full_fn2)
    F2 = 2*p2*r2/(p2+r2)
    p3 = full_tp3/(full_tp3+full_fp3)
    r3 = full_tp3/(full_tp3+full_fn3)
    F3 = 2*p3*r3/(p3+r3)
    p4 = full_tp4/(full_tp4+full_fp4)
    r4 = full_tp4/(full_tp4+full_fn4)
    F4 = 2*p4*r4/(p4+r4)
    print('overall F1, F2, F3, F4 is', F1, F2, F3, F4)
    

