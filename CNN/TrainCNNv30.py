# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 23:03:39 2017

@author: user
"""
name = 'gpu_25x25x3_rotation'
import dataInputNoPIL as dataInput
import numpy as np
import tensorflow as tf
from CNN import *
sess = tf.InteractiveSession()

###### SET PARAMETERS
trainingStepSize = 1e-6
numIterations = 60000
sizeBatches = 100
proportionSynapses = 0.05
n = 12 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 1#vertical length is 2n+1

global threeD 
threeD = nz>0

####    LOAD ALL THE WEIGHTS AND BIASES

weights = np.load('/home/threeyp/mindblowing/saved_nets/weights_for_'+name+'.npz')
names = weights.items()[1][1]
values = weights.items()[0][1]
try:
    for i in range(len(names)):
        var_name = names[i]
        array = names[i]
        #exec(name + ' = values[i]')
        exec(var_name + ' = tf.Variable(values[i])')
except TypeError:
    names, values = values, names
    for i in range(len(names)):
        var_name = names[i]
        array = names[i]
        #exec(name + ' = values[i]')
        exec(var_name + ' = tf.Variable(values[i])')

###### SET UP CNN
##### DEFINE CNN
if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), 2*n+1, 2*n+1])#(2*n+1)**2])
#else:
#    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)

if threeD:
    x_image = tf.reshape(x, [-1,2*nz+1,2*n+1,2*n+1, 1])
#else:
#    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])

h_conv0 = tf.nn.relu(conv(x_image, W_conv0_o, threeD) + b_conv0_o)  #22x22
h_conv0 = tf.reshape(h_conv0, [-1,22,22,16])
threeD = 0
h_pooled0 = tf.nn.relu(tf.nn.conv2d(h_conv0, W_conv1_o, strides = [1, 2, 2, 1], padding = 'VALID'))            #11x11

h_conv2 = tf.nn.relu(conv(h_pooled0, W_conv2_o, threeD) + b_conv2_o) #8x8

h_conv_flat = tf.reshape(h_conv2, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv_flat, W_fc1_o) + b_fc1_o)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2_o) + b_fc2_o)

h_fc3 = tf.nn.relu(tf.matmul(h_fc2, W_fc3_o) + b_fc3_o)

y_conv = tf.nn.relu(tf.matmul(h_fc3, W_fc4_o) + b_fc4_o)

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(trainingStepSize).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.initialize_all_variables())




#### LOAD TRAINING DATA

EMimage = np.load("npy_train/image.npy")
synapses = np.load("npy_train/synapse.npy")
from time import time



def getOffset():
    #returns a vector betwwen 0 0 0 and 4 4 2
    #offsets = [(0,0,0), (2,2,1), (3,3,0), (3,1,1), (2,3,1), (1,1,0), (3,0,1), (1,3,0), (0,3,1),(0,1,1),(2,0,1),(0,2,0),(1,0,1)]
    #num = np.random.choice(len(offsets))
    #return offsets[num]
    offset = (np.random.choice(4), np.random.choice(4), np.random.choice(2))
    return offset

for i in range(numIterations):
  if i%10001 == 0:
      start = time()
      try:
          del synapseX
          del not_synapseX
      except:
          pass
      synapseX, not_synapseX = dataInput.createNeighbourSets(EMimage, synapses, n, nz = nz, steps = (4,4,2),offset=getOffset())
      print('neigbour sets in ', time()-start)
  batch = dataSampleRotate3D(not_synapseX, synapseX, sizeBatches, proportionSynapses, n, nz)			#should be dataSampleRotate3D
  if i%400 == 0:
    train_accuracy = accuracy.eval(feed_dict={x:batch[0], y1: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y1: batch[1], keep_prob: 0.5})

#### SAVE MODEL
to_save = []
names = []
weights = locals().copy()
for var_name in weights:
    if var_name[-2:] == '_o':
        exec('to_save.append(' + var_name + '.eval())')
        names.append(var_name)
        
np.savez('/home/threeyp/mindblowing/saved_nets/weights_for_'+name, to_save, names)
print('saved')
