""" gets accuracy on training data of 86.7%"""

from PIL import Image
import numpy as np
import tensorflow as tf
import random
import dataInput
sess = tf.InteractiveSession()


n = 5           #size of conv filter is 2n + 1

EMimage = dataInput.readTif("C:/Users/user/Python/envs/tensorflow/train/image.tif", 100)
synapse = dataInput.readTif("C:/Users/user/Python/envs/tensorflow/train/synapse.tif", 100)
synapseX, not_synapseX = dataInput.createNeighbourSets(EMimage, synapse, n, steps = (10,10,1))
del EMimage, synapse

not_synapseY = [[1,0]]*not_synapseX.shape[0]
synapseY = [[0,1]]*synapseX.shape[0]


def dataSample(not_synapseX, synapseX, numPoints):
    synSamples = numPoints//8
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*n+1)**2])
    for point in range(synSamples):
        data[point] = synapseX[synapseIndex[point]]
    for point in range(notSynSamples):
        data[synSamples+point] = not_synapseX[notSynapseIndex[point]]
    y = np.array([[0,1]]*synSamples+[[1,0]]*notSynSamples)

    return (data, y)


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')


x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y_ = tf.placeholder(tf.float32, shape = [None, 2])

#W_conv1 = weight_variable([3, 3, 1, 32])
W_conv1 = weight_variable([4, 4, 1, 32])
b_conv1 = bias_variable([32])

x_image = tf.reshape(x, [-1,11,11,1])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)    #is now 8x8(x32)

W_fc1 = weight_variable([8 * 8 * 32, 1024])
b_fc1 = bias_variable([1024])

h_conv1_flat = tf.reshape(h_conv1, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 2])
b_fc2 = bias_variable([2])

h_fc2 = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

W_fc3 = weight_variable([2, 2])
b_fc3 = bias_variable([2])

y_conv = tf.matmul(h_fc2, W_fc3) + b_fc3

print('training starts now matey')

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(2e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.global_variables_initializer())

for i in range(15000):
  batch = dataSample(not_synapseX, synapseX, 100)
  if i%100 == 0:
    train_accuracy = accuracy.eval(feed_dict={
        x:batch[0], y_: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})






######## make test sets
EMimageTEST = dataInput.readTif("C:/Users/user/Python/envs/tensorflow/train/image.tif", 1, fromBottom = True)
synapseTEST = dataInput.readTif("C:/Users/user/Python/envs/tensorflow/train/synapse.tif", 1, fromBottom = True)

testSynapsesX, testNotSynapsesX = dataInput.createNeighbourSets(EMimageTEST, synapseTEST, n)
test_not_synapseY = [[1,0]]*testNotSynapsesX.shape[0]
test_synapseY = [[0,1]]*testSynapsesX.shape[0]
XData = np.concatenate((testSynapsesX, testNotSynapsesX))
YData = np.concatenate((test_synapseY, test_not_synapseY))
    
PredMap = np.zeros([1014**2, 2])
for MIN in range(0, 1014**2, 39):
    MAX = MIN + 39
    pred = sess.run([y_conv], feed_dict={ x: XData[MIN:MAX], y_: YData[MIN:MAX], keep_prob: 1.0})[0]
    for index in range(MIN, MAX):
        PredMap[index] = pred[index%39]

RealMap = np.zeros([1014**2])
for i in range(0,len(YData)):
	if YData[i][1]:
		RealMap[i] = 1

for mult in [0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2]:
    TP=0
    FP=0
    FN=0
    TN=0
    for datum in range(1014**2):
        pred_syn = (PredMap[datum][1] > PredMap[datum][0]*mult)
        true_syn = YData[datum][1]
        if true_syn and pred_syn:
            TP += 1
        elif (not true_syn) and pred_syn:
            FP += 1
        elif true_syn and (not pred_syn):
            FN += 1
        elif (not true_syn) and (not pred_syn):
            TN += 1
    prec = TP/(TP+FP)
    rec = TP/(TP+FN)
    F1 = 2*(prec*rec)/(prec+rec)
    print("TP:", TP, "FP:", FP, "FN:", FN, "TN:", TN, " so F1 is", F1, "for mult of", mult)