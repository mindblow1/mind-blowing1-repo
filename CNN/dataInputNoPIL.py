# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 15:40:02 2017

@author: user
"""
#from PIL import Image
import numpy as np

def createNeighbourSets(EMimage, synapses, n, steps = (1,1,1), nz = 0, zeroPadding=False, offset = (0,0,0)):
    """ Creates a numpy array of the neighbours in the (2*n+1)*(2*nz+1) grid around each pixel,
    moving in steps given by 'steps'. Ignores all the layers up to the 'firstLayer' """
    #EMimage and synapses should both be numpy arrays of same dimensions
    assert (EMimage.shape == synapses.shape)
    x_step, y_step, z_step = steps
    depth, height, width = EMimage.shape
    
    if zeroPadding:
        numDataPoints = int(np.ceil((height-offset[1])/y_step)*np.ceil((width-offset[0])/x_step)*np.ceil((depth-offset[2])/z_step))
    else:
        numDataPoints = int(np.ceil((height-2*n-offset[1])/y_step)*np.ceil((width-2*n-offset[0])/x_step)*np.ceil((depth-2*nz-offset[2])/z_step))
    dimensions = (2*n+1)**2*(2*nz+1)
    
    if zeroPadding:
        x_1, y_1, z_1 = offset[0], offset[1], offset[2]
        x_L, y_L, z_L = width, height, depth
    else:
        x_1, y_1, z_1 = offset[0]+n, offset[1]+n, offset[2]+nz
        x_L, y_L, z_L = width-n, height-n, depth-nz
    
    #count the synapses
    numSynapses = 0
    for k in range(z_1, z_L, z_step):
        for j in range(y_1, y_L, y_step):
            for i in range(x_1, x_L, x_step):
                if synapses[k][j][i]:
                    numSynapses += 1
    
    Synapses = np.zeros([numSynapses, dimensions])
    try:
        notSynapses = np.zeros([numDataPoints-numSynapses, dimensions])
    except:
        print(numDataPoints, numSynapses)
        raise ValueError
    synapsesAdded = 0
    notSynapsesAdded = 0
    for k in range(z_1, z_L, z_step):
        for j in range(y_1, y_L, y_step):
            for i in range(x_1, x_L, x_step):
                #compile a list of all the neighbours of the pixel
                
                
                neighbours = EMimage[k-nz:k+nz+1, j-n:j+n+1, i-n:i+n+1].reshape([-1])     ## doesn't work for zero padding
                            
                if synapses[k][j][i]:
                    Synapses[synapsesAdded] = neighbours
                    synapsesAdded += 1
                else:
                    notSynapses[notSynapsesAdded] = neighbours
                    notSynapsesAdded += 1
                    
    assert(synapsesAdded == numSynapses)
    assert(notSynapsesAdded == numDataPoints-numSynapses)
    
    return (Synapses, notSynapses)

""" some tests """
if __name__ == '__main__':
    from time import time
    EMimage = np.load("npy_train/image.npy")
    synapses = np.load("npy_train/synapse.npy")
    start = time()
    a = createNeighbourSets(EMimage, synapses, 12, nz = 1, steps = (20,20,20))
    print(time()-start)
    del EMimage, synapses
