# -*- coding: utf-8 -*-
"""
How many itersions should we do? testing with a 5x5 filter, 2e-4 step size
"""

import dataInputNoPIL as dataInput
import numpy as np
import random
import tensorflow as tf
sess = tf.InteractiveSession()

def calcF1(synapse_predictions, not_synapse_predictions, prob = 0.5):
    """synapse predictions is an array of probabilities that each real synapse is a syanpse
    not_synapse_predictions is for each non-synapse
    prob is the threshold probability to make the prediciton"""
    TP = sum(synapse_predictions>prob)[0]
    FN = sum(synapse_predictions<=prob)[0]
    FP = sum(not_synapse_predictions>prob)[0]
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1 = 2*precision*recall/(precision+recall)
    return F1

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

def dataSample(not_synapseX, synapseX, numPoints, proportionSynapse):
    #returns array of neighbours and y data for randomly selected points
    synSamples = int(numPoints*proportionSynapse)
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*n+1)**2])
    for point in range(synSamples):
        data[point] = synapseX[synapseIndex[point]]
    for point in range(notSynSamples):
        data[synSamples+point] = not_synapseX[notSynapseIndex[point]]
    y = np.array([1]*synSamples+[0]*notSynSamples)

    return (data, y)

def makeDoubleVector(y1):
    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
    y1 = tf.reshape(y1, [-1, 1])
    yNot = tf.logical_not(tf.cast(y1, tf.bool))
    yNot = tf.cast(yNot, tf.float32)
    return tf.concat(1, [yNot, y1])

###### SET PARAMETERS
numIterations = 100000
sizeBatches = 100
proportionSynapses = 0.05
n = 5 #filter is side length n+1                     -best to adjust the 2*n-6 on line 68


###### LOAD DATA

EMimage = np.load("npy_train/image.npy")
synapses = np.load("npy_train/synapse.npy")
synapseX, not_synapseX = dataInput.createNeighbourSets(EMimage, synapses, n, steps = (4,4,1))
del EMimage, synapses

###### DEFINE CNN
x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)

W_conv1 = weight_variable([2*n-6, 2*n-6, 1, 32])            #should really adjust size with n
b_conv1 = bias_variable([32])

x_image = tf.reshape(x, [-1,2*n+1,2*n+1,1])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)    #is now 8x8(x32)

W_fc1 = weight_variable([8 * 8 * 32, 1024])
b_fc1 = bias_variable([1024])

h_conv1_flat = tf.reshape(h_conv1, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 2])
b_fc2 = bias_variable([2])

h_fc2 = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

W_fc3 = weight_variable([2, 2])
b_fc3 = bias_variable([2])

y_conv = tf.matmul(h_fc2, W_fc3) + b_fc3

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(2e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.global_variables_initializer())

###### RUN TRAINING

for i in range(numIterations):
  batch = dataSample(not_synapseX, synapseX, sizeBatches, proportionSynapses)
  if i%100 == 0:
    train_accuracy = accuracy.eval(feed_dict={x:batch[0], y1: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y1: batch[1], keep_prob: 0.5})

  if i%10000 == 9999:
    print('\nAFTER', i, 'ITERATIONS:\n')
    ###### OPTIMISE P TO TAKE SYNAPSE AT FOR BEST F1 SCORE ON TRAINING DATA
    synapse_predictions = sess.run([probSynapse], feed_dict={ x: synapseX, y1: [1]*len(synapseX), keep_prob: 1.0})[0]
    not_synapse_predictions = sess.run([probSynapse], feed_dict={ x: not_synapseX, y1: [0]*len(not_synapseX), keep_prob: 1.0})[0]
    
    bestF1, bestProb = 0, 0
    for prob in np.arange(0.05,1,0.05):
        F1 = calcF1(synapse_predictions, not_synapse_predictions, prob)
        if F1 > bestF1:
            bestF1, bestProb = F1, prob
    print('for training data at best prob:', bestProb, 'F1:', bestF1, '\n\n\n')
    
    
    ###### LOAD TEST DATA
    imageTEST = np.load("npy_train/imageTEST.npy")
    synapsesTEST = np.load("npy_train/synapseTEST.npy")
    test_synapseX, test_not_synapseX = dataInput.createNeighbourSets(imageTEST, synapsesTEST, n)
    test_synapse_predictions = sess.run([probSynapse], feed_dict={ x: test_synapseX, y1: [1]*len(test_synapseX), keep_prob: 1.0})[0]
    test_not_synapse_predictions = sess.run([probSynapse], feed_dict={ x: test_not_synapseX, y1: [0]*len(test_not_synapseX), keep_prob: 1.0})[0]
    
    
    ######## CALCULATE F1 ON TEST DATA
    TP = sum(test_synapse_predictions>bestProb)[0]
    FN = sum(test_synapse_predictions<=bestProb)[0]
    FP = sum(test_not_synapse_predictions>bestProb)[0]
    TN = sum(test_not_synapse_predictions<=bestProb)[0]
    F1 = calcF1(test_synapse_predictions, test_not_synapse_predictions, bestProb)
    print('for test data at best prob, TP:', TP, 'FN:', FN, 'FP:', FP, 'TN:', TN, 'F1:', F1, '\n\n\n')
