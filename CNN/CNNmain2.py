# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 17:30:13 2017

@author: user

a lot slower than CNNmain, even when nz = 0
"""


import dataInputNoPIL as dataInput
import numpy as np
import random
import tensorflow as tf
sess = tf.InteractiveSession()

###### SET PARAMETERS
numIterations = 50000
sizeBatches = 100
proportionSynapses = 0.05
n = 5 #filter is side length 2n+1                     -best to adjust the 2*n-6 on line 68
nz = 1#vertical length is 2n+1

def getPredictions(sess, xData, n, nz):
    if threeD:
        xData = np.reshape(xData, [-1, (2*nz+1), (2*n+1)**2])
    #note it doesn't matter what y1 is set to
    return sess.run([probSynapse], feed_dict={ x: xData, y1: [0]*len(xData), keep_prob: 1.0})[0]
    
def calcF1(synapse_predictions, not_synapse_predictions, prob = 0.5, andMore = False):
    """synapse predictions is an array of probabilities that each real synapse is a syanpse
    not_synapse_predictions is for each non-synapse
    prob is the threshold probability to make the prediciton"""
    TP = sum(synapse_predictions>prob)[0]
    FN = sum(synapse_predictions<=prob)[0]
    FP = sum(not_synapse_predictions>prob)[0]
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1 = 2*precision*recall/(precision+recall)
    if andMore:
        return (F1, TP, FN, FP)
    else:
        return F1

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv(x, W):
    if threeD:
        return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='VALID')
    else:
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')


def dataSample(not_synapseX, synapseX, numPoints, proportionSynapse):
    #returns array of neighbours and y data for randomly selected points
    synSamples = int(numPoints*proportionSynapse)
    synapseIndex = random.sample(range(len(synapseX)), synSamples)
    notSynSamples = numPoints-synSamples
    notSynapseIndex = random.sample(range(len(not_synapseX)), notSynSamples)
    #take random numbers w/out replacement up to len(not_synapseX)

    data = np.zeros([numPoints, (2*nz+1)*(2*n+1)**2])
    for point in range(synSamples):
        data[point] = synapseX[synapseIndex[point]]
    for point in range(notSynSamples):
        data[synSamples+point] = not_synapseX[notSynapseIndex[point]]
    y = np.array([1]*synSamples+[0]*notSynSamples)
        
    if threeD:
        data = np.reshape(data, [-1, (2*nz+1), (2*n+1)**2])
    return (data, y)

def makeDoubleVector(y1):
    #extends y from array of [1 0 1 0 ...] to [[0, 1],[1, 0],[0, 1],[1, 0] ...]
    y1 = tf.reshape(y1, [-1, 1])
    yNot = tf.logical_not(tf.cast(y1, tf.bool))
    yNot = tf.cast(yNot, tf.float32)
    return tf.concat(1, [yNot, y1])

global threeD 
threeD = nz>0

###### LOAD DATA
EMimage = np.load("npy_train/image.npy")
synapses = np.load("npy_train/synapse.npy")
synapseX, not_synapseX = dataInput.createNeighbourSets(EMimage, synapses, n, nz = nz, steps = (5,5,1))
del EMimage, synapses

###### DEFINE CNN
if threeD:
    x = tf.placeholder(tf.float32, shape = [None, (2*nz+1), (2*n+1)**2])
else:
    x = tf.placeholder(tf.float32, shape = [None, (2*n+1)**2])
y1 = tf.placeholder(tf.float32, shape = [None])
y_ = makeDoubleVector(y1)

if threeD:
    W_conv1 = weight_variable([2*n-6, 2*n-6, 2*nz+1, 1, 32])            #should really adjust size with n
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1,2*nz+1, 1])
else:
    W_conv1 = weight_variable([2*n-6, 2*n-6, 1, 32])            #should really adjust size with n
    x_image = tf.reshape(x, [-1,2*n+1,2*n+1, 1])
b_conv1 = bias_variable([32])

h_conv1 = tf.nn.relu(conv(x_image, W_conv1) + b_conv1)    #is now 8x8(x32)

W_fc1 = weight_variable([8 * 8 * 32, 1024])
b_fc1 = bias_variable([1024])

h_conv1_flat = tf.reshape(h_conv1, [-1, 8*8*32])
h_fc1 = tf.nn.relu(tf.matmul(h_conv1_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 2])
b_fc2 = bias_variable([2])

h_fc2 = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

W_fc3 = weight_variable([2, 2])
b_fc3 = bias_variable([2])

y_conv = tf.matmul(h_fc2, W_fc3) + b_fc3

probSynapse = tf.nn.softmax(y_conv)   #gives vector of probabilities that each point is a synapse
probSynapse = tf.slice(probSynapse, [0, 1], [-1, 1]) #only take second column
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
train_step = tf.train.AdamOptimizer(2e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.global_variables_initializer())

###### RUN TRAINING

for i in range(numIterations):
  batch = dataSample(not_synapseX, synapseX, sizeBatches, proportionSynapses)
  if i%100 == 0:
    train_accuracy = accuracy.eval(feed_dict={x:batch[0], y1: batch[1], keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: batch[0], y1: batch[1], keep_prob: 0.5})


###### OPTIMISE THRESHOLD PROBABILITY TO TAKE SYNAPSE AT FOR BEST F1 SCORE ON TRAINING DATA
synapse_predictions = getPredictions(sess, synapseX, n, nz)#sess.run([probSynapse], feed_dict={ x: synapseX, y1: [1]*len(synapseX), keep_prob: 1.0})[0]
not_synapse_predictions = getPredictions(sess, not_synapseX, n, nz)#sess.run([probSynapse], feed_dict={ x: not_synapseX, y1: [0]*len(not_synapseX), keep_prob: 1.0})[0]

bestF1, bestProb = 0, 0
for prob in np.arange(0.05,1,0.05):
    F1 = calcF1(synapse_predictions, not_synapse_predictions, prob)
    if F1 > bestF1:
        bestF1, bestProb = F1, prob
print('for training data at best prob:', bestProb, 'F1:', bestF1, '\n\n\n')


###### LOAD TEST DATA
imageTEST = np.load("npy_train/imageTEST25.npy")
synapsesTEST = np.load("npy_train/synapseTEST25.npy")
test_synapseX, test_not_synapseX = dataInput.createNeighbourSets(imageTEST, synapsesTEST, n, nz=nz, steps = (2,2,1))
test_synapse_predictions = getPredictions(sess, test_synapseX, n, nz)#sess.run([probSynapse], feed_dict={ x: test_synapseX, y1: [1]*len(test_synapseX), keep_prob: 1.0})[0]
test_not_synapse_predictions = getPredictions(sess, test_not_synapseX, n, nz)#sess.run([probSynapse], feed_dict={ x: test_not_synapseX, y1: [0]*len(test_not_synapseX), keep_prob: 1.0})[0]

######## CALCULATE F1 ON TEST DATA
F1, TP, FN, FP = calcF1(test_synapse_predictions, test_not_synapse_predictions, bestProb, andMore=True)
print('for test data at best prob, TP:', TP, 'FN:', FN, 'FP:', FP, 'F1:', F1, '\n\n\n')

for prob in np.arange(0.05,1,0.05):
    print('on test data at prob =', prob, 'F1 =', calcF1(test_synapse_predictions, test_not_synapse_predictions, prob))
