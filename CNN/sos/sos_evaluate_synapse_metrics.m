function [precision, recall, F1] = sos_evaluate_metrics(detected,set)
% Takes inputs of found synapses and ground truth synapses.
% Detected - n:4 matrix in form of unique ID, x, y, z co-ords of centroid.
% Set - 'train', 'validate', or 'test' accordingly.
% Returns precision, recall, and F1 score.

% Set radius for valid synapse
detection_range = 15;
true_positive = 0;
false_positive = 0;
false_negative = 0;


if strcmp(set,'train')
    load('train_synapses');
else if strcmp(set, 'validate')
        load('validate_synapses');
    else if strcmp(set, 'test')
            load('test_synapses');
        end
    end
end


for i = 1:size(detected,1)
    found = false;
    
    for j = 1:size(contained_synapses,1)
        % Calculate distance from coordinates.
        guess = double(detected(i,2:4));
        target = double(contained_synapses(j,2:4));
        distance = norm(guess-target);
        
        if distance < detection_range
            % Remove synapse as 'used'.
            contained_synapses(j,:) = []; 
            % Found a good synapse.
            true_positive = true_positive + 1; 
            found = true;
            break
        end
    end

    if ~found
        % No synapse found so your synapse is wrong...
        false_positive = false_positive + 1;       
    end
    
end

false_negative = size(contained_synapses,1);

% Calculate metrics.
precision = true_positive / (true_positive + false_positive);
recall = true_positive / (true_positive + false_negative);
F1 = 2 * precision * recall / (precision + recall);

end