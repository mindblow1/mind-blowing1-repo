\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Chemical Processing - Fixation}{10}{0}{2}
\beamer@subsectionintoc {2}{1}{Necessity of Chemical Process}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Outline of Chemical Process}{12}{0}{2}
\beamer@subsectionintoc {2}{3}{Fixation}{13}{0}{2}
\beamer@sectionintoc {3}{Thick-sectioning}{19}{0}{3}
\beamer@sectionintoc {4}{Chemical Process - Staining and Embedding}{20}{0}{4}
\beamer@subsectionintoc {4}{1}{Staining}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{Embedding}{24}{0}{4}
\beamer@subsectionintoc {4}{3}{Automation}{27}{0}{4}
\beamer@subsectionintoc {4}{4}{Time and cost consideration}{29}{0}{4}
