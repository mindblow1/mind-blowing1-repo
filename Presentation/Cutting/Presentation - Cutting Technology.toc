\beamer@sectionintoc {1}{Thick Sectioning}{3}{0}{1}
\beamer@sectionintoc {2}{Ultrathin Sectioning}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Automated Tape-collecting Ultramicrotome}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Focused Ion Beam Milling}{17}{0}{2}
\beamer@sectionintoc {3}{Conclusion}{22}{0}{3}
