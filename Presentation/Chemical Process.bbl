\begin{thebibliography}{10}

\bibitem{celegan}
Cornelia~I Bargmann and Eve Marder.
\newblock From the connectome to brain function.
\newblock {\em Nature Methods}, 10(6):483--490, may 2013.

\bibitem{mouseretina}
Kevin~L. Briggman, Moritz Helmstaedter, and Winfried Denk.
\newblock Wiring specificity in the direction-selectivity circuit of the
  retina.
\newblock {\em Nature}, 471(7337):183--188, mar 2011.

\bibitem{plasticity}
Marian~C. Diamond, David Krech, and Mark~R. Rosenzweig.
\newblock The effects of an enriched environment on the histology of the rat
  cerebral cortex.
\newblock {\em The Journal of Comparative Neurology}, 123(1):111--119, 1964.

\bibitem{dykstra}
Michael~J Dykstra.
\newblock {\em Biology electron microscopy}.
\newblock Plenum Press, 1 edition, 1992.

\bibitem{hayworthreview}
KENNETH~J. HAYWORTH.
\newblock Electron imaging technology for whole brain neural circuit mapping.
\newblock {\em International Journal of Machine Consciousness}, 04(01):87--108,
  2012.

\bibitem{seedb2}
Meng-Tsen Ke, Yasuhiro Nakai, Satoshi Fujimoto, Rie Takayama, Shuhei Yoshida,
  Tomoya~S. Kitajima, Makoto Sato, and Takeshi Imai.
\newblock Super-resolution mapping of neuronal circuitry with an
  index-optimized clearing agent.
\newblock {\em Cell Reports}, 14(11), 2016.

\bibitem{mapseq}
Justus~M. Kebschull, Pedro Garcia~da Silva, Ashlan~P. Reid, Ian~D. Peikon,
  Dinu~F. Albeanu, and Anthony~M. Zador.
\newblock High-throughput mapping of single-neuron projections by sequencing of
  barcoded rna.
\newblock {\em Neuron}, 91(5):975--987, 2016.

\bibitem{mcintyre}
R.~L. McIntyre and G.~M. Fahy.
\newblock Aldehyde-stabilized cryopreservation.
\newblock {\em Cryobiology}, 71(3):448--458, 2015.

\bibitem{bropa}
Shawn Mikula and Winfried Denk.
\newblock High-resolution whole-brain staining for electron microscopic circuit
  reconstruction.
\newblock {\em Nature Methods}, 12(6):541--546, 2015.

\bibitem{seung}
Sebastian Seung.
\newblock {\em Connectome}.
\newblock Penguin, 1 edition, 2013.

\bibitem{spurr}
Arthur~R. Spurr.
\newblock A low-viscosity epoxy resin embedding medium for electron microscopy.
\newblock {\em Journal of Ultrastructure Research}, 26(1):31 -- 43, 1969.

\bibitem{brainbow1}
T.~A. Weissman, J.~R. Sanes, J.~W. Lichtman, and J.~Livet.
\newblock Generating and imaging multicolor brainbow mice.
\newblock {\em Cold Spring Harbor Protocols}, 2011(7), 2011.

\end{thebibliography}
