\beamer@sectionintoc {1}{Chemical Processing}{3}{0}{1}
\beamer@sectionintoc {2}{Cutting}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Underlying problems}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Microtome}{6}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Vibratome}{9}{0}{2}
\beamer@subsubsectionintoc {2}{2}{2}{Ultramicrotome}{12}{0}{2}
\beamer@subsectionintoc {2}{3}{Focused Ion Beam Milling}{18}{0}{2}
\beamer@subsectionintoc {2}{4}{Data recording}{22}{0}{2}
\beamer@subsubsectionintoc {2}{4}{1}{Conclusion}{25}{0}{2}
\beamer@sectionintoc {3}{Recording Technologies}{27}{0}{3}
\beamer@sectionintoc {4}{Data Processing}{28}{0}{4}
